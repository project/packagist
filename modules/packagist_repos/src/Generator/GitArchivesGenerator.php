<?php

namespace Drupal\packagist_repos\Generator;

use Drupal\packagist\Backend\PackagistBackend;

/**
 * Class GitArchivesGenerator.
 */
class GitArchivesGenerator implements GitArchivesGeneratorInterface {

  protected $packagist_backend;

  /**
   * Constructs a new GitArchivesGenerator object.
   */
  public function __construct(PackagistBackend $packagist_backend) {
    $this->packagist_backend = $packagist_backend;
  }

  /**
   * Run 'git clone' action.
   */
  public function runGitClone($git_repos_path, $repository_url, $repository_dir_name, $connection_params = []) {
    $action_args = [
      'git_repos_path' => $git_repos_path,
      'repository_url' => $repository_url,
      'repository_dir_name' => $repository_dir_name,
    ];
    $status_info = $this->packagist_backend->runAction('git_clone', $action_args, NULL, NULL, $connection_params);
    return $status_info;
  }

  /**
   * Run 'git fetch' action.
   */
  public function runGitFetch($git_repos_path, $repository_dir_name, $connection_params = []) {
    $action_args = [
      'git_repos_path' => $git_repos_path,
      'repository_dir_name' => $repository_dir_name,
    ];
    $status_info = $this->packagist_backend->runAction('git_fetch', $action_args, NULL, NULL, $connection_params);
    return $status_info;
  }

  /**
   * Run 'git snapshot update' action.
   *
   * @todo: $tag_name can be removed from arguments if not used, see the
   *   commented code below
   */
  public function runGitSnapshotUpdate($git_repos_path, $repository_dir_name, $tag_name = NULL, $connection_params = []) {

    /*
    // make it easier to skip parameter value when calling the method from outside
    if (NULL == $tag_name) {
      $tag_name = 'HEAD';
    }
    */

    // use git_list_tags and composer_json_by_commit_ref backend actions to get snapshot data

    $action_args = [
      'git_repos_path' => $git_repos_path,
      'repository_dir_name' => $repository_dir_name,
    ];

    $status_info_tags = $this->packagist_backend->runAction('git_list_tags', $action_args, NULL, NULL, $connection_params);

    // @todo: check status code
    $status_info = [
      'status' => $status_info_tags['status'],
      'message' => $status_info_tags['message'],
      'data' => [
        'tags' => $status_info_tags['data'],
      ],
    ];

    // The vendor_name and project_name values are set manually using field
    // widget form instead of requesting and parsing composer.json "name"
    // property.
    // see the respective comment in Repository::runGitSnapshotUpdate()
    /*
    $action_args['tag_name'] = $tag_name;

    // @todo: check status for errors
    $status_info_name = $this->packagist_backend->runAction('composer_json_by_commit_ref', $action_args, NULL, FALSE, $connection_params);
    $status_info_name = json_decode($status_info_name['data'], TRUE);
    // @todo: consider the case when there is no composer.json file or it cannot be parsed
    //   or 'name' key is not defined etc.
    list($vendor_name, $project_name) = explode('/', $status_info_name['name']);
    $status_info['data']['vendor_name'] = $vendor_name;
    $status_info['data']['project_name'] = $project_name;
    */

    return $status_info;
  }

  /**
   * Run 'get composer json by commit ref' action.
   */
  public function runComposerJsonByCommitRef($git_repos_path, $repository_dir_name, $tag_name, $connection_params = []) {
    $action_args = [
      'git_repos_path' => $git_repos_path,
      'repository_dir_name' => $repository_dir_name,
      'tag_name' => $tag_name,
    ];

    $status_info = $this->packagist_backend->runAction('composer_json_by_commit_ref', $action_args, NULL, FALSE, $connection_params);

    return $status_info;
  }

  /**
   * Run 'generate archives' action.
   */
  public function runGenerateArchives($git_repos_path, $repository_dir_name, $archives_path, $package_archives_dir_name, $tags_file_names, $connection_params = []) {

    $action = 'generate_archive';

    // @todo: iterate through tags to generate archives and prepare new parameters
    // @todo: the tags should be obtained from package_config[imported][tags] with 'publish' set to "1" (to true)

    // @todo: use archives_base_url to prepare archive file full url
    //   though no need to deal with archives_base_url here - should be done on plugin level


    foreach ($tags_file_names as $tag_name => $archive_file_name) {
      //$archive_dir_name = "{$vendor_name}/{$package_name}";
      $action_args = [
        'archives_path' => $archives_path,
        'tag_name' => $tag_name,
        'archive_file_name' => $archive_file_name,
        'package_archives_dir_name' => $package_archives_dir_name,
        'git_repos_path' => $git_repos_path,
        'repository_dir_name' => $repository_dir_name,
      ];
      // @todo: get tags archives from action_status
      //   should be added in handler plugin method
      $status_info = $this->packagist_backend->runAction($action, $action_args, NULL, NULL, $connection_params);
    }

    return $status_info;
  }

  // @todo: move action names into constants to be reusable all around the code
  // @todo: maybe rename 'composer_json_by_commit_ref' to 'get_package_json_by_tag'

  // @todo: add to base class and to interface

}
