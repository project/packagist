<?php

namespace Drupal\packagist_repos\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\packagist_repos\Entity\RepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Access\AccessResult;

/**
 * Controller for repository package actions ajax requests.
 */
class RepositoryAjaxActionsController extends ControllerBase {

  // @todo: add 'exclude/unpublish', 'remove archives' and 'unexpose/rollback' actions
  const ACTION_UPDATE_STATUS = 'status';
  const ACTION_GIT_CLONE = 'git_clone';
  const ACTION_GIT_FETCH = 'git_fetch';
  const ACTION_GIT_CLONE_UPDATE = 'clone_update';
  const ACTION_GIT_FETCH_UPDATE = 'fetch_update';
  const ACTION_BUILD_EXPOSE = 'build_expose';

  const ACTION_UPDATE_SNAPSHOT = 'git_snapshot_update';
  const ACTION_IMPORT_TAGS = 'import_tags';
  const ACTION_INCLUDE_TAGS = 'include_tags';
  const ACTION_EXCLUDE_TAGS = 'exclude_tags';
  const ACTION_BUILD = 'build_all';
  const ACTION_EXPOSE = 'expose';

  // @todo: for actions like BUILD_EXPOSE it is possible to use batch operations
  //   on UI side (if needed)

  /**
   * Return response with action execution status info.
   *
   * @return JsonResponse
   *   Return status_info response.
   */
  public function response(RepositoryInterface $packagist_repository, $action) {
    switch ($action) {
      case static::ACTION_UPDATE_STATUS :
      case static::ACTION_GIT_CLONE :
      case static::ACTION_GIT_FETCH :
      //case static::ACTION_GIT_CLONE_UPDATE :
      //case static::ACTION_GIT_FETCH_UPDATE :
      //case static::ACTION_BUILD_EXPOSE :
      case static::ACTION_UPDATE_SNAPSHOT :
      case static::ACTION_IMPORT_TAGS :
      case static::ACTION_INCLUDE_TAGS :
      case static::ACTION_EXCLUDE_TAGS :
      case static::ACTION_BUILD :
      case static::ACTION_EXPOSE :
        $response = $this->runRepositoryAction($packagist_repository, $action);
        break;
      default:
        // @todo: emit an error
        //   or return 404 response
        $response = [
          'status' => '0',
          'message' => t('Action not supported'),
        ];
    }

    return new JsonResponse($response);
  }

  /**
   * Access handler for response() method.
   */
  public function access(RepositoryInterface $packagist_repository, $action) {
    // get current user account
    $account = \Drupal::currentUser();

    // @todo: check if user has access to *all* repositories, i.e. allow to bypass access check
    //   check if user has access to view the entity
    $is_owner = $account->id() == $packagist_repository->getOwnerId();
    return AccessResult::allowedIf($is_owner
      && $account->hasPermission('access packagist repository actions'));
  }

  /**
   * Get response array for the action.
   */
  public function runRepositoryAction($packagist_repository, $action) {
    // @todo: check permissions for the user and given entity
    // @todo: check if the entity belongs to the current user
    //   add check to the access check callback

    // @todo: make changes to the resultant structure if needed,
    //   explicitly list response data keys since it actually
    //   describes the api conventions used on the client-side,
    //   also some validation could be appropriate here

    // @todo: message should be set here based on status code
    //   also the status code to be replaced here by text as well
    switch ($action) {
      case static::ACTION_UPDATE_STATUS :
        $status_info = $packagist_repository->runGetStatus();
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          'data' => $status_info['data'],
        ];
        break;
      // @todo: add git_fetch action to update repository
      //   also check for non-fast-forward changes, removed/new tags and branches,
      //   other significant changes and data
      case static::ACTION_GIT_CLONE :
        $status_info = $packagist_repository->runGitClone();
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          'data' => $status_info['data'],
        ];
        break;
      case static::ACTION_GIT_FETCH :
        $status_info = $packagist_repository->runGitFetch();
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          //'data' => $status_info['data'],
        ];
        break;
      case static::ACTION_UPDATE_SNAPSHOT :
        $status_info = $packagist_repository->runGitSnapshotUpdate();
        // @todo: though these status and message are for client-side which are
        //   actually different (by its sense from status and message used internally
        //   since the action is about "to update snapshot" but to about "to get data"
        //   which is done using "status" action)
        //   and should be defined by js api definition
        unset($status_info['data']);
        // just return status and message
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          //'data' => $status_info['data'],
        ];
        break;
      case static::ACTION_IMPORT_TAGS :
        $status_info = $packagist_repository->runImportTags();
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          'data' => $status_info['data'],
        ];
        break;
      // @todo: maybe rename to publish (though can cause confusion with
      //   generating packagist json structure)
      case static::ACTION_INCLUDE_TAGS :
        $status_info = $packagist_repository->runIncludeTags();
        // just return status and message
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          //'data' => $status_info['data'],
        ];
        break;
      // @todo: the action is not currently used
      case static::ACTION_EXCLUDE_TAGS :
        $status_info = $packagist_repository->runExcludeTags();
        // just return status and message
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          //'data' => $status_info['data'],
        ];
        break;
      case static::ACTION_GIT_CLONE_UPDATE :
        // @todo:
        break;
      // @todo: the action is not currently used
      case static::ACTION_GIT_FETCH_UPDATE :
        // @todo:
        break;
      // @todo: the action is not currently used
      case static::ACTION_BUILD_EXPOSE :
        // @todo:
        // @todo: this would return $response array though only $status_info is needed
        //   thus move the given method code into a new method
        // @todo: this should return action-specific response based on the two actions result
        $response = $this->response($packagist_repository, static::ACTION_BUILD);
        $response = $this->response($packagist_repository, static::ACTION_EXPOSE);
        break;
      case static::ACTION_BUILD :
        $status_info = $packagist_repository->runGenerateArchives();
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          'data' => $status_info['data'],
        ];
        break;
      // @todo: maybe rename to 'update_definition' for 'update package definition'
      case static::ACTION_EXPOSE :
        $status_info = $packagist_repository->runExpose();
        $response = [
          'status' => $status_info['status'],
          'message' => $status_info['message'],
          'data' => $status_info['data'],
        ];
        break;
      default:
        // @todo: emit an error
        //   or return 404 response
        $response = [
          'status' => '0',
          'message' => t('Action not supported'),
        ];
    }

    return $response;
  }

}
