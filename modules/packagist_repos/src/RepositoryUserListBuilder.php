<?php

namespace Drupal\packagist_repos;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Repository User entities.
 *
 * @ingroup packagist_repos
 */
class RepositoryUserListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Repository User ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\packagist_repos\Entity\RepositoryUser $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.packagist_repository_user.edit_form',
      ['packagist_repository_user' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
