<?php

namespace Drupal\packagist_repos\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'packagist_repository' formatter.
 *
 * @FieldFormatter(
 *   id = "packagist_repository",
 *   label = @Translation("Repository"),
 *   field_types = {
 *     "packagist_repository"
 *   }
 * )
 */
class RepositoryFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'packagist_repository_formatter_ui',
        '#field_name' => $items->getName(),
        '#repo_id' => $items->getEntity()->id(),
        '#delta' => $delta,
      ];
    }
    $elements['#attached']['library'][] = 'packagist_repos/repository-formatter-ui';

    return $elements;
  }

}
