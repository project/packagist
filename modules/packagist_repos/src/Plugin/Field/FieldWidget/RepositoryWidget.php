<?php

namespace Drupal\packagist_repos\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'packagist_repository' widget.
 *
 * @FieldWidget(
 *   id = "packagist_repository",
 *   module = "packagist_repos",
 *   label = @Translation("Repository"),
 *   field_types = {
 *     "packagist_repository"
 *   }
 * )
 */
class RepositoryWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'fieldset';
    $element['repository_url'] = [
      '#type' => 'textfield',
      '#title' => t('Repository url'),
      '#default_value' => isset($items[$delta]->repository_url) ? $items[$delta]->repository_url : NULL,
      '#size' => '64',
      '#placeholder' => 'https://github.com/symfony/symfony.git',
      '#description' => t('The URL of the Git repository. Used on backend to clone repository files.'),
    ];
    $element['directory_name'] = [
      '#type' => 'textfield',
      '#title' => t('Directory name'),
      '#default_value' => isset($items[$delta]->directory_name) ? $items[$delta]->directory_name : NULL,
      '#size' => '32',
      '#placeholder' => 'symfony',
      '#description' => t('The directory name to be used on backend for cloning repository into. It is relative to the packagist <em>Git repositories path</em>. Must be unique for each repository in the packagist.'),
    ];

    // @todo: maybe just use foreach loop
    //   and use hidden or value (should be editable via ui, used only for development)
    $element['operational_status'] = [
      '#type' => 'value',
      '#title' => t('Operational status'),
      '#value' => isset($items[$delta]->operational_status) ? $items[$delta]->operational_status : NULL,
    ];
    $element['repository_snapshot'] = [
      '#type' => 'value',
      '#title' => t('Repository snapshot'),
      '#value' => isset($items[$delta]->repository_snapshot) ? $items[$delta]->repository_snapshot : NULL,
    ];
    $element['package_config'] = [
      '#type' => 'value',
      '#title' => t('Packages configuration'),
      '#value' => isset($items[$delta]->package_config) ? $items[$delta]->package_config : NULL,
    ];
    $element['archives_snapshot'] = [
      '#type' => 'value',
      '#title' => t('Archives snapshot'),
      '#value' => isset($items[$delta]->archives_snapshot) ? $items[$delta]->archives_snapshot : NULL,
    ];
    $element['package_json_snapshot'] = [
      '#type' => 'value',
      '#title' => t('Package json snapshot'),
      '#value' => isset($items[$delta]->package_json_snapshot) ? $items[$delta]->package_json_snapshot : NULL,
    ];
    $element['composer_vendor_name'] = [
      // @todo: use textfields for dev purposes, convert to 'value' type
      '#type' => 'textfield',
      '#title' => t('Vendor name'),
      '#default_value' => $items[$delta]->composer_vendor_name,
      '#description' => t('The value is defined based on composer.json contents.'),
      '#required' => TRUE,
      // @todo: only allow to change if not set
      //'#disabled' => TRUE,
    ];
    $element['composer_project_name'] = [
      '#type' => 'textfield',
      '#title' => t('Project name'),
      '#default_value' => $items[$delta]->composer_project_name,
      '#description' => t('The value is defined based on composer.json contents.'),
      '#required' => TRUE,
      // @todo: only allow to change if not set
      //'#disabled' => TRUE,
    ];

    return $element;
  }

}
