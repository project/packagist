<?php

namespace Drupal\packagist_repos\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'packagist_repository' field type.
 *
 * @FieldType(
 *   id = "packagist_repository",
 *   label = @Translation("Repository"),
 *   description = @Translation("Provides git repositories support"),
 *   default_widget = "packagist_repository",
 *   default_formatter = "packagist_repository"
 * )
 */
class RepositoryItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    // @todo: check for required settings, i.e. case_sensitive setting
    $properties['repository_url'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Repository url'))
      ->setRequired(TRUE);
    $properties['directory_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Directory name'))
      ->setRequired(TRUE);

    $properties['operational_status'] = DataDefinition::create('string')
      ->setLabel(t('Operational status'));
    $properties['repository_snapshot'] = DataDefinition::create('string')
      ->setLabel(t('Repository snapshot'));
    $properties['package_config'] = DataDefinition::create('string')
      ->setLabel(t('Package configuration'));
    $properties['archives_snapshot'] = DataDefinition::create('string')
      ->setLabel(t('Archives snapshot'));
    $properties['package_json_snapshot'] = DataDefinition::create('string')
      ->setLabel(t('Package json snapshot'));

    $properties['composer_vendor_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Composer vendor name'))
      ->setRequired(FALSE);
    $properties['composer_project_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Composer project name'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'repository_url' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'directory_name' => [
          'type' => 'varchar',
          'length' => '255',
        ],

        // @todo: maybe change type to longtext
        'operational_status' => [
          'type' => 'text',
          // @todo: for now use text for values to be visibile when using database queries
          'mysql_type' => 'text',
          //'mysql_type' => 'blob',
          'description' => 'The status data on performed operations, e.g. last status check or cloned status and time.',
        ],
        'repository_snapshot' => [
          'type' => 'text',
          'size' => 'medium',
          //'size' => 'big',
          // @todo: for now use text for values to be visibile when using database queries
          //'mysql_type' => 'mediumtext',
          //'mysql_type' => 'mediumblob',
          'description' => 'All the data about repository obtained from bash script.',
        ],

        // @todo: review data key name, e.g. packages_setup
        'package_config' => [
          'type' => 'text',
          'size' => 'medium',
          // @todo: for now use text for values to be visibile when using database queries
          //'mysql_type' => 'mediumtext',
          //'mysql_type' => 'mediumblob',
          'description' => 'Configuration to be used for creating and updating packages archives and packages.json structure',
        ],

        'archives_snapshot' => [
          'type' => 'text',
          'size' => 'medium',
          // @todo: for now use text for values to be visibile when using database queries
          //'mysql_type' => 'mediumtext',
          //'mysql_type' => 'mediumblob',
          'description' => 'All the data about package versions archives generated for the package.',
        ],

        // @todo: review data key name, change to package_definition_snapshot
        'package_json_snapshot' => [
          'type' => 'text',
          'size' => 'medium',
          // @todo: for now use text for values to be visibile when using database queries
          //'mysql_type' => 'mediumtext',
          //'mysql_type' => 'mediumblob',
          'description' => 'All the data about actual package definition generated files status.',
        ],

        // @todo: the properties values are defined based on repository composer.json contents,
        //   maybe allow to set manually
        'composer_vendor_name' => [
          'type' => 'varchar',
          'length' => 64,
        ],
        'composer_project_name' => [
          'type' => 'varchar',
          'length' => 64,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // @todo: also check directory and other properties
    $value = $this->get('repository_url')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * Get operation status data.
   */
  public function getOperationalStatus() {
    $operational_status = $this->get('operational_status')->getValue();
    if (!empty($operational_status)) {
      $operational_status = unserialize($operational_status);
    }
    else {
      $operational_status = [
        'is_cloned' => 0,
      ];
    }
    return $operational_status;
  }

  /**
   * Set operation status data.
   */
  public function setOperationalStatus($operational_status) {
    $this->set('operational_status', serialize($operational_status));
    return $this;
  }

  /**
   * Get package configuration data.
   */
  public function getPackageConfig() {
    $package_config = $this->get('package_config')->getValue();
    if (!empty($package_config)) {
      $package_config = unserialize($package_config);
    }
    else {
      $package_config = [
        'imported' => [
          'tags' => [],
        ],
      ];
    }
    return $package_config;
  }

  /**
   * Set package configuration data.
   */
  public function setPackageConfig($package_config) {
    $this->set('package_config', serialize($package_config));
    return $this;
  }

  /**
   * Get repository snapshot data.
   */
  public function getRepositorySnapshot() {
    $repository_snapshot = $this->get('repository_snapshot')->getValue();
    if (!empty($repository_snapshot)) {
      $repository_snapshot = unserialize($repository_snapshot);
    }
    else {
      $repository_snapshot = [
        'tags' => [],
      ];
    }
    return $repository_snapshot;
  }

  /**
   * Set repository snapshot data.
   */
  public function setRepositorySnapshot($repository_snapshot) {
    $this->set('repository_snapshot', serialize($repository_snapshot));
    return $this;
  }

}
