<?php

namespace Drupal\packagist_repos\Plugin\Packagist\PackagistHandler;

use Drupal\packagist\Core\PackagistHandlerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\packagist_repos\Entity\Repository;

/**
 * Provides a 'Git' Packagist Handler.
 *
 * @PackagistHandler(
 *  id = "git",
 *  label = @Translation("Git")
 * )
 */
class GitPackagistHandler extends PackagistHandlerBase {

  protected $git_archives_generator;

  protected $packagist_generator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->git_archives_generator = \Drupal::service('packagist_repos.git_archives.generator');
    $this->packagist_generator = \Drupal::service('packagist.generator');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration += [
      'git_repos_path' => '',
      'archives_path' => '',
      'archives_base_url' => '',
    ];

    return $default_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function runAction($action, $parameters) {
    // @todo: different actions may require different generator services
    //   though all those services rely on one core backend service and bash script for now

    switch ($action) {
      case Repository::HANDLER_ACTION_GIT_CLONE :
        // @todo: check if values are set and valid
        $git_repos_path = $this->getReposPathFull();
        $repository_url = $parameters['repository_url'];
        $repository_dir_name = $parameters['repository_dir_name'];

        $status_info = $this->git_archives_generator->runGitClone($git_repos_path, $repository_url, $repository_dir_name);
        break;
      case Repository::HANDLER_ACTION_GIT_FETCH :
        // @todo: check if values are set and valid
        $git_repos_path = $this->getReposPathFull();
        $repository_dir_name = $parameters['repository_dir_name'];

        $status_info = $this->git_archives_generator->runGitFetch($git_repos_path, $repository_dir_name);
        break;
      case Repository::HANDLER_ACTION_GIT_UPDATE_SNAPSHOT :
        // @todo: check if values are set and valid
        $git_repos_path = $this->getReposPathFull();
        $repository_dir_name = $parameters['repository_dir_name'];
        $status_info = $this->git_archives_generator->runGitSnapshotUpdate($git_repos_path, $repository_dir_name);
        if ($status_info['status'] == 1) {
          // @todo: also check status codes
        }
        break;
      case Repository::HANDLER_ACTION_GENERATE_ARCHIVES :
        // @todo: check if values are set and valid
        $git_repos_path = $this->getReposPathFull();
        $repository_dir_name = $parameters['repository_dir_name'];
        $archives_path = $this->getArchivesPathFull();
        $package_archives_dir_name = $parameters['package_archives_dir_name'];

        $tags_file_names = [];
        // @todo: it is supposed that only zip archives are generated for now,
        //   so is supposed in server-core.sh script as well, though the 'type' could
        //   be set as parameter and checked in the script later
        // prepare package archives file names for tags
        foreach ($parameters['tags'] as $tag_name) {
          $tags_file_names[$tag_name] = $this->getArchiveFileNameByTag($tag_name);
        }

        // @todo: maybe return archives file names as part of status_info, since
        //   may be needed to store in repository archives_snapshot field property
        //   or provide a public ::getArchiveFileNameByTag() method that could be
        //   used on demand from the repository entity methods
        //   as it is done when generating packagist json structure below (i.e.
        //   relying on the method to get files names instead of storing them in
        //   repository field property)

        $status_info = $this->git_archives_generator->runGenerateArchives($git_repos_path, $repository_dir_name, $archives_path, $package_archives_dir_name, $tags_file_names);
        break;
      case Repository::HANDLER_ACTION_GENERATE_PACKAGIST_JSON_STRUCTURE :
        // @todo: check if values are set and valid
        $archives_base_url = $this->configuration['archives_base_url'];
        $git_repos_path = $this->getReposPathFull();
        $repository_dir_name = $parameters['repository_dir_name'];

        $package_tags_schema = [];
        $vendor_name = $parameters['vendor_name'];
        $project_name = $parameters['project_name'];

        // get composer.json contents structure for each of the tags
        // and add additional values, e.g. dist url and type for corresponding archives
        if (!empty($parameters['tags'])) {
          $tags = $parameters['tags'];
          sort($tags);
          foreach ($tags as $tag_name) {
            // @todo: check status for errors
            $status_info = $this->git_archives_generator->runComposerJsonByCommitRef($git_repos_path, $repository_dir_name, $tag_name);
            if ($status_info['status'] == 1) {
              // @todo: check status codes
              $composer_json = $status_info['data'];
              $package_tags_schema[$tag_name] = json_decode($composer_json, TRUE);

              // @todo: this shouldn't be used for dev-master and other branches sources
              //   since branch versions dist do not use archives
              $archive_file_name = $this->getArchiveFileNameByTag($tag_name);
              $archive_file_url = "$archives_base_url/{$vendor_name}/{$project_name}/{$archive_file_name}";
              // Add 'dist' info for the package tag version.
              // Some other properties may be added in packagist.genertor service,
              // e.g. 'uid' property.
              $package_tags_schema[$tag_name]['dist'] = [
                'url' => $archive_file_url,
                'type' => 'zip',
              ];
            }
          }
        }

        // update packagist json structure
        if (!empty($package_tags_schema)) {
          $packages = [];
          $packages[$vendor_name][$project_name] = $package_tags_schema;

          // @todo: get full base_path value
          $base_path = $this->getBasePathFull();
          $base_url = $this->configuration['base_url'];

          // @todo: get from parameters or better from repository entity,
          //   which should be set in parameters or as context, or as method argument
          $packagist_id = $parameters['packagist_id'];
          // @todo: needs to also set archive source info
          $status_info = $this->packagist_generator
            ->updatePackagistStructure($packages, $base_path, $base_url, $packagist_id);
        }
        else {
          // @todo:
          $status_info = [
            'status' => '0',
          ];
        }
        break;
      default:
        // @todo:
        $status_info = [
          'status' => '0',
        ];
    }

    // @todo: return meaningful data in a unified format, make changes if needed.
    //   final client-side api-related changes should be done on controller level
    return $status_info;
  }

  protected function getBasePathFull() {
    $base_path = $this->configuration['base_path'];
    if (strpos($base_path, '/') !== 0) {
      $base_path = DRUPAL_ROOT . '/' . $base_path;
    }
    return $base_path;
  }

  protected function getReposPathFull() {
    $git_repos_path = $this->configuration['git_repos_path'];
    if (strpos($git_repos_path, '/') !== 0) {
      $git_repos_path = DRUPAL_ROOT . '/' . $git_repos_path;
    }
    return $git_repos_path;
  }

  protected function getArchivesPathFull() {
    $archives_path = $this->configuration['archives_path'];
    if (strpos($archives_path, '/') !== 0) {
      $archives_path = DRUPAL_ROOT . '/' . $archives_path;
    }
    return $archives_path;
  }

  // @todo: add git_repos_dir to the handler config form
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // @todo: review settings key names, e.g. git_base_path or repos_base_path
    // @todo: should old repos be removed on the given path changed?
    //   or maybe the directory itself moved to the new location?
    //   or should it be somehow defined in settings?
    //   or maybe mark as temporary and remove after some period of time or on new
    //   repos cloned to new location for the first time?
    $form['git_repos_path'] = [
      '#type' => 'textfield',
      '#title' => t('Git repositories path'),
      '#default_value' => $this->configuration['git_repos_path'],
      '#required' => TRUE,
    ];
    $form['archives_path'] = [
      '#type' => 'textfield',
      '#title' => t('Archives path'),
      '#default_value' => $this->configuration['archives_path'],
      '#required' => TRUE,
    ];
    $form['archives_base_url'] = [
      '#type' => 'url',
      '#title' => t('Archives base url'),
      '#default_value' => $this->configuration['archives_base_url'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Return zip archive file name based on tag name.
   */
  protected function getArchiveFileNameByTag($tag_name, $archive_type = 'zip') {
    // @todo: $archive_type is not check, only 'zip' archives are generated for now
    return "{$tag_name}.zip";
  }

}
