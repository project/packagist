<?php

namespace Drupal\packagist_repos\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Repository User entity.
 *
 * @ingroup packagist_repos
 *
 * @ContentEntityType(
 *   id = "packagist_repository_user",
 *   label = @Translation("Repository User"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\packagist_repos\RepositoryUserListBuilder",
 *     "views_data" = "Drupal\packagist_repos\Entity\RepositoryUserViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\packagist_repos\Form\RepositoryUserForm",
 *       "add" = "Drupal\packagist_repos\Form\RepositoryUserForm",
 *       "edit" = "Drupal\packagist_repos\Form\RepositoryUserForm",
 *       "delete" = "Drupal\packagist_repos\Form\RepositoryUserDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\packagist_repos\RepositoryUserHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\packagist_repos\RepositoryUserAccessControlHandler",
 *   },
 *   base_table = "packagist_repository_user",
 *   translatable = FALSE,
 *   admin_permission = "administer repository user entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/packagist/repository/user/{packagist_repository_user}",
 *     "add-form" = "/admin/packagist/repository/user/add",
 *     "edit-form" = "/admin/packagist/repository/user/{packagist_repository_user}/edit",
 *     "delete-form" = "/admin/packagist/repository/user/{packagist_repository_user}/delete",
 *     "collection" = "/admin/packagist/repository/user",
 *   },
 *   field_ui_base_route = "packagist_repository_user.settings"
 * )
 */
class RepositoryUser extends ContentEntityBase implements RepositoryUserInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Repository User entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Repository User entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Repository User is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    // @todo: review field settings
    // add packagist_repository_ref field
    $fields['repository'] = BaseFieldDefinition::create('packagist_repository_ref')
      ->setLabel(t('Repository'))
      ->setDescription(t('The repositories that user has access to with respective access settings.'))
      /*
      ->setSetting('target_type', 'packagist_repository')
      ->setSetting('handler', 'default')
      */
      //->setSetting('handler_settings',['target_bundles'=>['packagist_repository'=>'packagist_repository']])
      ->setDisplayOptions('view', array(
        'label'  => 'hidden',
        'type'   => 'packagist_repository_ref',
        //'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'packagist_repository_ref',
        'weight'   => 5,
        /*
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
        */
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(FALSE);

    return $fields;
  }

}
