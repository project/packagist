<?php

// @todo: also group repositories by username (or allow such a setting)
//   same for archives and packagists - would be requried for multiuser packagists
//   and also to avoid conflicts when one repo defines a dir_name being already in use

namespace Drupal\packagist_repos\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Repository entity.
 *
 * @ingroup packagist_repos
 *
 * @ContentEntityType(
 *   id = "packagist_repository",
 *   label = @Translation("Repository"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\packagist_repos\RepositoryListBuilder",
 *     "views_data" = "Drupal\packagist_repos\Entity\RepositoryViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\packagist_repos\Form\RepositoryForm",
 *       "add" = "Drupal\packagist_repos\Form\RepositoryForm",
 *       "edit" = "Drupal\packagist_repos\Form\RepositoryForm",
 *       "delete" = "Drupal\packagist_repos\Form\RepositoryDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\packagist_repos\RepositoryHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\packagist_repos\RepositoryAccessControlHandler",
 *   },
 *   base_table = "packagist_repository",
 *   translatable = FALSE,
 *   admin_permission = "administer repository entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/packagist/repository/{packagist_repository}",
 *     "add-form" = "/admin/packagist/repository/add",
 *     "edit-form" = "/admin/packagist/repository/{packagist_repository}/edit",
 *     "delete-form" = "/admin/packagist/repository/{packagist_repository}/delete",
 *     "collection" = "/admin/packagist/repository",
 *   },
 *   field_ui_base_route = "packagist_repository.settings"
 * )
 */
class Repository extends ContentEntityBase implements RepositoryInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  const DEFAULT_FIELD_PACKAGIST_REF = 'packagist';
  const DEFAULT_FIELD_REPOSITORY =  'repository';

  // use 'action_' prefix to distinguish from controller action names
  const HANDLER_ACTION_GIT_CLONE = 'action_git_clone';
  const HANDLER_ACTION_GIT_FETCH = 'action_git_fetch';
  const HANDLER_ACTION_GIT_UPDATE_SNAPSHOT = 'action_git_snapshot_update';
  const HANDLER_ACTION_GENERATE_ARCHIVES = 'action_generate_archives';
  const HANDLER_ACTION_GENERATE_PACKAGIST_JSON_STRUCTURE = 'action_generate_packagist_json_structure';

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Repository entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Repository entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Repository is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    // add Packagist reference field
    $fields['packagist'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Packagist'))
      ->setDescription(t('The packagist ID, repository packages belong to.'))
      ->setSetting('target_type', 'packagist')
      ->setSetting('handler', 'default')
      //->setSetting('handler_settings', ['target_bundles' => ['packagist' => 'packagist']] )
      ->setDisplayOptions('view', array(
        'label'  => 'hidden',
        'type'   => 'packagist',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    // @todo: review field settings
    // @todo: add packagist_repository field widget and formatter settings
    $fields['repository'] = BaseFieldDefinition::create('packagist_repository')
      ->setLabel(t('Repository'))
      ->setDescription(t('The repository url, local directory name, operational status and other data used to build and manage the package.'))
      ->setSettings([
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'packagist_repository',
        //'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'packagist_repository',
        //'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    return $fields;
  }


  // @todo: check if field is not empty
  protected function getPackagistItem($field_name = self::DEFAULT_FIELD_PACKAGIST_REF, $delta = 0) {
    return $this->{$field_name}->get($delta);
  }

  // @todo: check if field is not empty
  protected function getRepositoryFieldItem($field_name = self::DEFAULT_FIELD_REPOSITORY, $delta = 0) {
    return $this->{$field_name}->get($delta);
  }


  // @todo: check user permissions to perform actions and packagist ownership

  // @todo: prefix all actions with 'action_' prefix

  // @todo: for now just return $action_status as is
  //   since changes should be done on topmost calling level,
  //   and even leave it so as convention

  // @todo: maybe check for available backend version compatibility

  /**
   * Run 'get status' action.
   */
  public function runGetStatus() {
    $repository_field_item = $this->getRepositoryFieldItem();

    // @todo: get repository_snapshot and other fields info
    $repository_snapshot = $repository_field_item->getRepositorySnapshot();
    $package_config = $repository_field_item->getPackageConfig();
    $action_status = [
      'status' => '1',
      'message' => 'ok',
      'data' => [
        'snapshot' => $repository_snapshot,
        // @todo: review key names
        'package_config' => $package_config,
      ],
    ];

    return $action_status;
  }

  /**
   * Run 'git clone repository' action.
   */
  public function runGitClone() {
    $action = static::HANDLER_ACTION_GIT_CLONE;

    $repository_field_item = $this->getRepositoryFieldItem();
    $parameters = [
      'repository_url' => $repository_field_item->repository_url,
      'repository_dir_name' => $repository_field_item->directory_name,
    ];
    $action_status = $this->doRunPackagistHandlerAction($action, $parameters);

    // @todo: also possible to check if already is_cloned to avoid running action again
    $operational_status = $repository_field_item->getOperationalStatus();
    // @todo: check action status
    // add is_cloned flag to operational status
    $operational_status['is_cloned'] = 1;
    $repository_field_item->setOperationalStatus($operational_status);
    $this->save();

    return $action_status;
  }

  /**
   * Run 'git fetch repository' action.
   */
  public function runGitFetch() {
    $repository_field_item = $this->getRepositoryFieldItem();
    // check if repository is cloned before fetching
    // @todo: also check on script level
    $operational_status = $repository_field_item->getOperationalStatus();
    if (!empty($operational_status['is_cloned'])) {
      $action = static::HANDLER_ACTION_GIT_FETCH;
      $parameters = [
        'repository_dir_name' => $repository_field_item->directory_name,
      ];
      $action_status = $this->doRunPackagistHandlerAction($action, $parameters);
    }
    else {
      // @todo:
      $action_status = [
        'status' => '0',
        'message' => 'error',
      ];
    }

    return $action_status;
  }

  /**
   * Run 'git make repository snapshot' action.
   */
  public function runGitSnapshotUpdate() {

    // @todo: do not allow to make snapshot if repository isn't cloned
    //   or composer.json is not there or any other errors occur

    $repository_field_item = $this->getRepositoryFieldItem();
    // check if repository is cloned before getting its snapshot
    // @todo: also check on script level, e.g. if repository is removed
    //   maybe add 'reclone' action for that case
    $operational_status = $repository_field_item->getOperationalStatus();
    if (!empty($operational_status['is_cloned'])) {
      // @todo: maybe this shouldn't run HANDLER_ACTION_GIT_UPDATE_SNAPSHOT,
      //   but instead first get tags and then other actions if needed separately
      $action = static::HANDLER_ACTION_GIT_UPDATE_SNAPSHOT;

      // @todo: also check operational status and if repository is cloned
      $parameters = [
        'repository_dir_name' => $repository_field_item->directory_name,
      ];
      $action_status = $this->doRunPackagistHandlerAction($action, $parameters);

      // @todo: check $action_status['status']
      $repository_snapshot = $repository_field_item->getRepositorySnapshot();
      $repository_snapshot = [
        'tags' => $action_status['data']['tags'],
      ];
      // @todo: actually the data element should contain all the snapshot data
      //$repository_snapshot = $action_status['data'];

      $repository_field_item->setRepositorySnapshot($repository_snapshot);

      // The vendor_name and project_name values are set manually using field
      // widget form instead of requesting and parsing composer.json "name"
      // property.
      // @todo: the behavior may be reenabled later

      /*
      // @todo: this may be empty, also allow to set manually via edit form if needed
      //   and do not override then
      // @todo: this uses HEAD ref for composer.json file contents
      // set composer_vendor_name and composer_project_name fields values
      $repository_field_item->composer_vendor_name = $action_status['data']['vendor_name'];
      $repository_field_item->composer_project_name = $action_status['data']['project_name'];
      */
      $this->save();
    }
    else {
      // @todo:
      $action_status = [
        'status' => '0',
        'message' => 'error',
      ];
    }


    return $action_status;
  }


  /**
   * Run 'import tags' action.
   */
  public function runImportTags() {
    $repository_field_item = $this->getRepositoryFieldItem();

    // @todo: compare tags with those from snapshot, i.e. tags can't be included
    //   if they are not present in snapshot
    // @todo: validate new_tags (check that is array)
    $new_tags = \Drupal::request()->query->get('tags', []);
    $repository_snapshot = $repository_field_item->getRepositorySnapshot();
    $snapshot_tags = $repository_snapshot['tags'];
    $tags = [];
    $package_config = $repository_field_item->getPackageConfig();
    $imported_tags = $package_config['imported']['tags'];
    foreach ($new_tags as $new_tag) {
      if (in_array($new_tag, $snapshot_tags) && !in_array($new_tag, $imported_tags)) {
        $tags[] = $new_tag;
      }
    }

    $tags_info = $imported_tags;
    foreach ($tags as $tag) {
      if (empty($new_tag)) {
        continue;
      }
      // @todo: merge with existing info, do not override existing 'publish' values,
      //   only and new ones (actually it is always supposed to be new ones but the
      //   check and array_intersect is also required for tags list)
      $tags_info[$tag] = [
        'publish' => 0,
      ];
    }

    // @todo: merge with existing info
    // @todo: sort tags by name
    $package_config['imported']['tags'] = $tags_info;
    $repository_field_item->setPackageConfig($package_config);

    // @todo: lock parallel operations or maybe on controller level
    $this->save();

    $action_status = [
      'status' => '1',
      'message' => 'ok',
      'data' => [
        'tags' => $new_tags,
      ],
    ];

    return $action_status;
  }

  /**
   * Run 'include tags' action.
   */
  public function runIncludeTags() {
    $repository_field_item = $this->getRepositoryFieldItem();

    // @todo: compare tags with those from imported, i.e. tags can't be included
    //   if they are not present in imported
    // @todo: validate new_tags (check that is array)
    $new_tags = \Drupal::request()->query->get('tags', []);
    $package_config = $repository_field_item->getPackageConfig();
    $imported_tags = $package_config['imported']['tags'];
    foreach ($new_tags as $new_tag) {
      if (empty($new_tag)) {
        continue;
      }
      // @todo: maybe use some other value instead of '0' since it is still a string
      //   and may return true in checks, or make explicit (int) type
      if (isset($imported_tags[$new_tag]) && !$imported_tags[$new_tag]['publish']) {
        $imported_tags[$new_tag]['publish'] = 1;
      }
    }


    // @todo: move everything into field type method

    $package_config['imported']['tags'] = $imported_tags;

    $repository_field_item->setPackageConfig($package_config);

    // @todo: lock parallel operations or maybe on controller level
    $this->save();

    $action_status = [
      'status' => '1',
      'message' => 'ok',
    ];

    return $action_status;
  }

  /**
   * Run 'exclude tags' action.
   */
  public function runExcludeTags() {
    $repository_field_item = $this->getRepositoryFieldItem();

    // @todo: compare tags with those from included, i.e. tags can't be excluded
    //   if they are not present in included
    // @todo: validate unpublish_tags (check that is array)
    $unpublish_tags = \Drupal::request()->query->get('tags', []);
    $package_config = $repository_field_item->getPackageConfig();
    $imported_tags = $package_config['imported']['tags'];
    foreach ($unpublish_tags as $unpublish_tag) {
      if (empty($unpublish_tag)) {
        continue;
      }
      // @todo: maybe use some other value instead of '0' since it is still a string
      //   and may return true in checks, or make explicit (int) type
      if (isset($imported_tags[$unpublish_tag]) && $imported_tags[$unpublish_tag]['publish']) {
        $imported_tags[$unpublish_tag]['publish'] = 0;
      }
    }


    // @todo: move everything into field type method

    $package_config['imported']['tags'] = $imported_tags;

    $repository_field_item->setPackageConfig($package_config);

    // @todo: lock parallel operations or maybe on controller level
    $this->save();

    $action_status = [
      'status' => '1',
      'message' => 'ok',
    ];

    return $action_status;
  }

  /**
   * Run 'generate package archives' action.
   */
  public function runGenerateArchives() {
    $repository_field_item = $this->getRepositoryFieldItem();

    //$directory_name = $repository_field_item->directory_name;

    // @todo: get parameters from query params
    // as here for git_clone $parameters['directory_name'] = $directory_name;

    // @todo: make sure the fields values are set
    // @todo: also make sure it doesn't change at subsequent snapshot updates
    $vendor_name = $repository_field_item->composer_vendor_name;
    $project_name = $repository_field_item->composer_project_name;



    // @todo: move into a method
    $package_config = $repository_field_item->getPackageConfig();
    $imported_tags = $package_config['imported']['tags'];
    $publish_tags = [];
    foreach ($imported_tags as $tag_name => $tag_info) {
      if ($tag_info['publish']) {
        $publish_tags[] = $tag_name;
      }
    }

    $action = static::HANDLER_ACTION_GENERATE_ARCHIVES;
    $parameters = [
      'repository_dir_name' => $repository_field_item->directory_name,
      // package archives are grouped by project_name inside corresponding vendor_name directory
      // @todo: also grouping by user and packagist should be done or at least
      //   a check should be added that packagist have different, non-intersecting
      //   base directories
      'package_archives_dir_name' => "{$vendor_name}/{$project_name}",
      'tags' => $publish_tags,
    ];

    // @todo: packagist knows nothing about the actions, maybe add a method
    //   to get handler plugin instance here and call it directly

    // only call if publish tags is not empty
    if ($publish_tags) {
      // @todo: get tags archives from action_status
      //   should be added in handler plugin method
      $action_status = $this->doRunPackagistHandlerAction($action, $parameters);
    }
    else {
      // @todo:
    }

    return $action_status;
  }


  /**
   * Run 'expose' action.
   */
  public function runExpose() {
    $packagist_item = $this->getPackagistItem();
    $repository_field_item = $this->getRepositoryFieldItem();
    $directory_name = $repository_field_item->directory_name;

    if (!empty($packagist_item)) {

      // @todo: move into a method
      $package_config = $repository_field_item->getPackageConfig();
      $imported_tags = $package_config['imported']['tags'];
      $publish_tags = [];
      // @todo: currently not used anywhere
      //$tags_sources = [];
      foreach ($imported_tags as $tag_name => $tag_info) {
        if ($tag_info['publish']) {
          $publish_tags[] = $tag_name;
        }
      }

      // @todo: check if packagist json structure update is needed
      //   i.e. if there are new tags to publish not yet having info provided in
      //   packagist json structure

      $action = static::HANDLER_ACTION_GENERATE_PACKAGIST_JSON_STRUCTURE;
      $parameters = [
        'repository_dir_name' => $directory_name,
        'vendor_name' => $repository_field_item->composer_vendor_name,
        'project_name' => $repository_field_item->composer_project_name,
        // @todo: should be obtained from repository entity inside the plugin
        //   when impelmented
        // @todo: is refrencing the entity needed to get the id? just get the field value
        'packagist_id' => $packagist_item->entity->id(),
        'tags' => $publish_tags,
      ];


      // @todo: also add archive path for each path
      //   also make sure archive is generated, i.e. add only those
      //   which have archives generated (as an option) though not needed for now
      // @todo: for now just set archives paramters and use them in the handler
      //   plugin to get/generate archives paths
      //   though this wouldn't work win 'dev-master' and similar branches source info
      //   actually would be correct to use 'archives_snapshot' value tags,
      //   i.e. to use only those tags which have generated archives, along with
      //   checking 'publish' key value
      $action_status = $this->doRunPackagistHandlerAction($action, $parameters);
    }

    return $action_status;
  }

  public function doRunPackagistHandlerAction($action, $parameters) {

    // @todo: make sure the packagist uses corresponding (i.e. Git) handler plugin, e.g. for clone,
    //   maybe introduce hanlder plugin types, or check that handler implements required interface
    $packagist_item = $this->getPackagistItem();
    if ($packagist_item) {
      $handler_plugin = $packagist_item->entity->getPackagistHandlerInstance();
      $action_status = $handler_plugin->runAction($action, $parameters);
    }
    else {
      // @todo:
      $action_status = [];
    }

    return $action_status;
  }

}
