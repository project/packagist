<?php

namespace Drupal\packagist_repos\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Repository entities.
 *
 * @ingroup packagist_repos
 */
interface RepositoryInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Repository name.
   *
   * @return string
   *   Name of the Repository.
   */
  public function getName();

  /**
   * Sets the Repository name.
   *
   * @param string $name
   *   The Repository name.
   *
   * @return \Drupal\packagist_repos\Entity\RepositoryInterface
   *   The called Repository entity.
   */
  public function setName($name);

  /**
   * Gets the Repository creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Repository.
   */
  public function getCreatedTime();

  /**
   * Sets the Repository creation timestamp.
   *
   * @param int $timestamp
   *   The Repository creation timestamp.
   *
   * @return \Drupal\packagist_repos\Entity\RepositoryInterface
   *   The called Repository entity.
   */
  public function setCreatedTime($timestamp);

}
