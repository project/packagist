<?php

namespace Drupal\packagist_repos\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Repository User entities.
 */
class RepositoryUserViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
