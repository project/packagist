<?php

namespace Drupal\packagist_repos\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Repository User entities.
 *
 * @ingroup packagist_repos
 */
interface RepositoryUserInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Repository User name.
   *
   * @return string
   *   Name of the Repository User.
   */
  public function getName();

  /**
   * Sets the Repository User name.
   *
   * @param string $name
   *   The Repository User name.
   *
   * @return \Drupal\packagist_repos\Entity\RepositoryUserInterface
   *   The called Repository User entity.
   */
  public function setName($name);

  /**
   * Gets the Repository User creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Repository User.
   */
  public function getCreatedTime();

  /**
   * Sets the Repository User creation timestamp.
   *
   * @param int $timestamp
   *   The Repository User creation timestamp.
   *
   * @return \Drupal\packagist_repos\Entity\RepositoryUserInterface
   *   The called Repository User entity.
   */
  public function setCreatedTime($timestamp);

}
