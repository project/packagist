<?php

namespace Drupal\packagist_repos;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Repository User entity.
 *
 * @see \Drupal\packagist_repos\Entity\RepositoryUser.
 */
class RepositoryUserAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\packagist_repos\Entity\RepositoryUserInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished repository user entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published repository user entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit repository user entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete repository user entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add repository user entities');
  }


}
