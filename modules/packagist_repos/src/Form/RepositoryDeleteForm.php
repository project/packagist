<?php

namespace Drupal\packagist_repos\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Repository entities.
 *
 * @ingroup packagist_repos
 */
class RepositoryDeleteForm extends ContentEntityDeleteForm {


}
