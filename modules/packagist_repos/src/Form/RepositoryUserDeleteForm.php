<?php

namespace Drupal\packagist_repos\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Repository User entities.
 *
 * @ingroup packagist_repos
 */
class RepositoryUserDeleteForm extends ContentEntityDeleteForm {


}
