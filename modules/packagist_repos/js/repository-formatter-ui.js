(function ($, Drupal, Vue, axios) {
  Drupal.behaviors.packagistReposFormatterUi = {
    attach: function (context, settings) {
      $('.js-packagist-repos-formatter-ui-app-wrapper', context).once('vueAppInit').each(function () {
        var fieldName = this.dataset.fieldName;
        var delta = this.dataset.delta;
        var repoId = this.dataset.repoId;

        var appSelectorId = '#js-packagist-repos-formatter-ui-'+fieldName+'-'+delta+'-'+repoId+'-app';

        initApp(appSelectorId, repoId);
      });
    }
  };
  function initApp(appSelectorId, repoId) {
    // @todo: use vue components

    var app = new Vue({
      el: appSelectorId,
      // use delimiters to avoid conflicts with twig
      delimiters: ['${', '}'],
      data: {
        repoId: repoId,
        vendor_name: '',
        project_name: '',
        snapshot: {
          filters: {
            name_pattern: ''
          },
          tags: [
            //'v1.0.0',
          ],
          tagsSelected: []
        },
        imported: {
          tags: {
            //'v1.0.0': { publish: true },
          },
          filters: {
          },
          tagsSelected: [
          ]
        },
        packaged: {
        },
        published: {
        },
        // @todo: show multiple messages in status bar, i.e. log of message (multiline)
        statusBar: {
          // @todo: translate it here or in template
          // @todo: set empty message and status by default
          messageText: 'Test message...',
          // other message types: warning, error
          messageType: 'status'
        },
        devMode: false
      },
      // @todo: also get data about generated archives
      //   and json packagist structure (i.e. actually published as json) tags
      computed: {
        snapshotTags: function () {
          var self = this;
          var snapshotTags = {};
          this.snapshot.tags.forEach(function(tagName) {
            var imported = false;
            if (typeof self.imported.tags[tagName] != 'undefined') {
              imported = true;
            }
            var selected = self.snapshot.tagsSelected.includes(tagName);
            snapshotTags[tagName] = { imported : imported, selected : selected };
          });
          return snapshotTags;
        },
        importedTags: function () {
          var self = this;
          var importedTags = {};
          for (let key in this.imported.tags) {
            if (this.imported.tags.hasOwnProperty(key)) {
              var tagName = key;
              var tagData = this.imported.tags[key];
              var selected = self.imported.tagsSelected.includes(tagName);
              importedTags[tagName] = { publish : !!tagData.publish, selected : selected };
            }
          }
          return importedTags;
        }
      },
      // @todo: maybe use 'created' instead
      mounted: function() {
        // @todo: maybe split into two methods
        this.onUpdateStatusClick();
      },
      methods: {
        // @todo: no need in repoId argument here
        // use beforeCallback() to do all required preliminary job
        runAction: function(action, repoId, params, successCallback, failCallback, finallyCallback, beforeCallback) {
          var repoId = this.repoId;
          // @todo: list allowed actions here

          // @todo: or get from settings
          var urlBase = '/packagist/repository/' + repoId + '/' + action;

          var beforeCallback = beforeCallback || null;
          if (beforeCallback) {
            // @todo: does it need this or self as argument?
            beforeCallback();
          }

          // @todo: implement default bahavior for failCallback() and finallyCallback()

          var self = this;
          axios.get(urlBase, {
              params: params
            })
            // @todo: this doesn't work if called directly without wrapping function()
            //.then(successCallback(response))
            .then(function (response) {
              successCallback(response);

              if (self.devMode) {
                console.log('success callback');
                console.log(response.data);
              }
            })
            .catch(function (error) {
              // handle error
              console.log(error);
            })
            .finally(function () {
              // @todo:
            });

          // @todo: return a promise object
          //   https://stackoverflow.com/questions/48980380/returning-data-from-axios-api/48980526
        },
        onUpdateStatusClick: function() {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            var data = response.data.data;
            var snapshot = data.snapshot;
            var tags = snapshot.tags;
            self.snapshot.tags = tags;

            var package_config = data.package_config;
            var imported = package_config.imported;
            self.imported.tags = imported.tags;

            // @todo: translate message text
            self.statusBar.messageText = 'updated status';
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'updating...';
          }
          this.runAction('status', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        onCloneRepositoryClick: function(successCallback, beforeCallback) {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = successCallback || function(response) {
            // @todo: it is also possible to perform git_snapshot_update and status update actions
            // @todo: set status bar message text and status
            //   also add failCallback() and finallyCallback() to set respective messages

            // @todo: translate message text
            self.statusBar.messageText = 'cloned';
          }
          var beforeCallback = beforeCallback || function() {
            // @todo: translate message text
            self.statusBar.messageText = 'cloning...';
          }
          this.runAction('git_clone', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        // @todo: update snapshot and update status after clone,
        //   do the same after fetch,
        //   also only do if needed
        onCloneAndUpdateClick: function() {
          // @todo: maybe use promises to avoid multiple nested calls

          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            // @todo: the message is immediately changed by 'update snapshot' action, i.e. not shown
            // @todo: translate message text
            self.statusBar.messageText = 'cloned';

            var successUpdateSnapshotCallback = function(response) {
              // @todo: translate message text
              self.statusBar.messageText = 'updated snapshot';

              // run 'update status' action on 'update snapshot' success
              self.onUpdateStatusClick();
            }

            var beforeUpdateSnapshotCallback = function() {
              // @todo: translate message text
              // @todo: move into a method, also pass status type as argument
              self.statusBar.messageText = 'updating snapshot...';
            }

            // run 'update snapshot' action on 'clone' success
            self.onUpdateSnapshotClick(successUpdateSnapshotCallback, beforeUpdateSnapshotCallback);
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'cloning...';
          }
          // @todo: override beforeCallback() if needed
          // @todo: pass successCallback override
          //   and call Update Snapshot callback there
          //   or maybe append/prepend to the Build action callback instead of override?
          this.onCloneRepositoryClick(successCallback, beforeCallback);


        },
        onFetchAndUpdateClick: function() {
          // mostly it just copies onCloneAndUpdateClick() method

          // @todo: maybe use promises to avoid multiple nested calls

          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            // @todo: the message is immediately changed by 'update snapshot' action, i.e. not shown
            // @todo: translate message text
            self.statusBar.messageText = 'fetched';

            var successUpdateSnapshotCallback = function(response) {
              // @todo: translate message text
              self.statusBar.messageText = 'updated snapshot';

              // run 'update status' action on 'update snapshot' success
              self.onUpdateStatusClick();
            }

            var beforeUpdateSnapshotCallback = function() {
              // @todo: translate message text
              // @todo: move into a method, also pass status type as argument
              self.statusBar.messageText = 'updating snapshot...';
            }

            // run 'update snapshot' action on 'fetch' success
            self.onUpdateSnapshotClick(successUpdateSnapshotCallback, beforeUpdateSnapshotCallback);
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'fetching...';
          }
          // @todo: override beforeCallback() if needed
          // @todo: pass successCallback override
          //   and call Update Snapshot callback there
          //   or maybe append/prepend to the Build action callback instead of override?
          this.onFetchRepositoryClick(successCallback, beforeCallback);
        },

        onFetchRepositoryClick: function(successCallback, beforeCallback) {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = successCallback || function(response) {
            // @todo: it is also possible to perform git_snapshot_update and status update actions

            // @todo: translate message text
            self.statusBar.messageText = 'fetched';
          }
          var beforeCallback = beforeCallback || function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'fetching...';
          }
          this.runAction('git_fetch', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        onUpdateSnapshotClick: function(successCallback, beforeCallback) {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = successCallback || function(response) {
            // @todo:

            // @todo: translate message text
            self.statusBar.messageText = 'updated snapshot';
          }
          var beforeCallback = beforeCallback || function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'updating snapshot...';
          }
          this.runAction('git_snapshot_update', this.repoId, params, successCallback, null, null, beforeCallback);
          //this.runAction('update_snapshot', this.repoId, params, successCallback);
        },
        onBuildAllAndExposeClick: function() {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            // @todo: the message is immediately changed by 'expose' action, i.e. not shown
            // @todo: translate message text
            self.statusBar.messageText = 'build complete';

            // run 'expose' action on 'build' success
            self.onExposeClick();
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'building...';
          }
          // @todo: override beforeCallback() if needed
          // @todo: pass successCallback override
          //   and call Expose callback there
          //   or maybe append/prepend to the Build action callback instead of override?
          this.onBuildAllClick(successCallback, beforeCallback);
        },
        // allow to override successCallback and beforeCallback callbacks
        onBuildAllClick: function(successCallback, beforeCallback) {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = successCallback || function(response) {
            // @todo:

            // @todo: translate message text
            self.statusBar.messageText = 'build complete';
          }

          // allow to override it
          var beforeCallback = beforeCallback || function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'building...';
          }
          // @todo: rename to build_all
          this.runAction('build_all', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        onExposeClick: function() {
          var self = this;
          var params = {};
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            // @todo:

            // @todo: translate message text
            self.statusBar.messageText = 'exposed';
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'exposing...';
          }
          this.runAction('expose', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        onSnapshotTagClick: function(tagName) {
          var tagsSelected = this.snapshot.tagsSelected;
          if (!tagsSelected.includes(tagName)) {
            tagsSelected.push(tagName);
          }
          else {
            var tagIndex = tagsSelected.indexOf(tagName);
            tagsSelected.splice(tagIndex, 1);
          }
        },
        onImportedTagClick: function(tagName) {
          var tagsSelected = this.imported.tagsSelected;
          if (!tagsSelected.includes(tagName)) {
            tagsSelected.push(tagName);
          }
          else {
            var tagIndex = tagsSelected.indexOf(tagName);
            tagsSelected.splice(tagIndex, 1);
          }
        },
        // @todo: review method name
        onSnapshotSelectAllVisibleTagsClick: function() {
          var self = this;
          this.snapshot.tags.forEach(function(tagName){
            if (!self.snapshot.tagsSelected.includes(tagName)) {
              self.snapshot.tagsSelected.push(tagName);
            }
          });
        },
        onSnapshotDeselectAllVisibleTagsClick: function() {
          this.snapshot.tagsSelected = [];
        },
        // @todo: review method name
        onSnapshotSelectedImportClick: function() {
          // @todo: get new (not earlier imported) tags list for the action
          var self = this;
          var newImportedTags = [];
          this.snapshot.tagsSelected.forEach(function(tagName) {
            // @todo: maybe add check though not needed
            if (typeof self.imported.tags[tagName] == 'undefined') {
              newImportedTags.push(tagName);
            }
          });

          // @todo: check newImportedTags if not empty and do nothing otherwise

          var params = {
            tags: newImportedTags
          };
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            // add imported tags to the list
            newImportedTags.forEach(function(tagName){
              // use $set to trigger computed values change
              // @todo: though also needs to sort by name, or at least to stick to snapshot
              //   tags order for now
              self.$set(self.imported.tags, tagName, { publish: false });
            });
            console.log(newImportedTags);

            // @todo: translate message text
            self.statusBar.messageText = 'imported';
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'importing...';
          }
          // @todo: only run action if there are new tags
          this.runAction('import_tags', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        onImportedSelectAllTagsClick: function() {
          var self = this;
          for (let key in this.imported.tags) {
            if (this.imported.tags.hasOwnProperty(key)) {
              var tagName = key;
              var tagData = this.imported.tags[key];
              if (!self.imported.tagsSelected.includes(tagName)) {
                self.imported.tagsSelected.push(tagName);
              }
            }
          }

        },
        onImportedDeselectAllTagsClick: function() {
          this.imported.tagsSelected = [];
        },
        onImportedSelectedPublishClick: function() {
          // @todo: get new (not earlier published) tags list for the action
          var self = this;
          var newPublishedTags = [];
          this.imported.tagsSelected.forEach(function(tagName) {
            // @todo: maybe add check though not needed
            if (!self.imported.tags[tagName].publish) {
              newPublishedTags.push(tagName);
            }
          });


          // @todo: also add to self.imported.published on success
          //   though "published" are those for which json structure is generated
          //   also use different names, these are confusing

          var params = {
            tags: newPublishedTags
          };
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {

            newPublishedTags.forEach(function(tagName){
              // @todo: here we use true/false though stored in database is 1/0 values
              self.imported.tags[tagName].publish = true;
            });
            console.log(newPublishedTags);

            // @todo: translate message text
            self.statusBar.messageText = 'published';
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'publishing...';
          }
          // @todo: only run action if there are new tags
          this.runAction('include_tags', this.repoId, params, successCallback, null, null, beforeCallback);
        },
        onImportedSelectedUnpublishClick: function() {
          // @todo: get new (earlier published) tags list for the action
          var self = this;
          var newUnublishedTags = [];
          this.imported.tagsSelected.forEach(function(tagName) {
            if (self.imported.tags[tagName].publish) {
              newUnublishedTags.push(tagName);
            }
          });

          // @todo: also remove from self.imported.published on success
          //   though "published" are those for which json structure is generated
          //   also use different names, these are confusing

          var params = {
            tags: newUnublishedTags
          };
          // @todo: also add failCallback and finallyCallback if needed
          var successCallback = function(response) {
            newUnublishedTags.forEach(function(tagName){
              // @todo: here we use true/false though stored in database is 1/0 values
              self.imported.tags[tagName].publish = false;
            });
            console.log(newUnublishedTags);

            // @todo: translate message text
            self.statusBar.messageText = 'unpublished';
          }
          var beforeCallback = function() {
            // @todo: translate message text
            // @todo: move into a method, also pass status type as argument
            self.statusBar.messageText = 'unpublishing...';
          }
          // @todo: only run action if there are tags to be excluded
          this.runAction('exclude_tags', this.repoId, params, successCallback, null, null, beforeCallback);
        }
      }
    });

  }
})(jQuery, Drupal, Vue, axios);
