<?php

/**
 * @file
 * Contains packagist_repository_user.page.inc.
 *
 * Page callback for Repository User entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Repository User templates.
 *
 * Default template: packagist_repository_user.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_packagist_repository_user(array &$variables) {
  // Fetch RepositoryUser Entity Object.
  $packagist_repository_user = $variables['elements']['#packagist_repository_user'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
