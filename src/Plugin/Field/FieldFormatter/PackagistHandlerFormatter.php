<?php

namespace Drupal\packagist\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'packagist_handler' formatter.
 *
 * @FieldFormatter(
 *   id = "packagist_handler",
 *   label = @Translation("Packagist handler"),
 *   field_types = {
 *     "packagist_handler"
 *   }
 * )
 */
class PackagistHandlerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // @todo:

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // @todo: add controls to perform packagist-specific actions,
    //   e.g. to regenerate all the packagist json scturcture
    $handler_info = [
      'handler_id' => $item->handler_id,
      // use getter method to get handler config array
      'handler_config' => $item->getHandlerConfig(),
    ];

    return '<pre>' . print_r($handler_info, 1) . '</pre>';
  }

}
