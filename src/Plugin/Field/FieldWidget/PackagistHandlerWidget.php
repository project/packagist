<?php

namespace Drupal\packagist\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of the 'packagist_handler' widget.
 *
 * @FieldWidget(
 *   id = "packagist_handler",
 *   module = "packagist",
 *   label = @Translation("Packagist handler"),
 *   field_types = {
 *     "packagist_handler"
 *   }
 * )
 */
class PackagistHandlerWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity = $items->getEntity();
    $bundle = $entity->bundle();
    $item = $items[$delta];

    $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();
    $bundle_config_entity = \Drupal::entityTypeManager()->getStorage($bundle_entity_type)->load($bundle);

    // check if $handler_id is defined for the bundle
    // get from field values if not isNew()
    $handler_id = $entity->isNew() ? $bundle_config_entity->getPackagistHandlerId()
                : $items[$delta]->handler_id;

    // $handler_id is expected to always be set
    // attach corresponding plugin config form if $handler_id set
    if (!empty($handler_id)) {
      $handler_label = \Drupal::service('plugin.manager.packagist_handler')->getDefinition($handler_id)['label'];

      $element['handler_markup'] = [
        '#markup' => t('Packagist handler: @handler_label', ['@handler_label' => $handler_label]),
      ];

      // store plugin_id in '#value', i.e. should not be changeable via ui
      $element['handler_id'] = [
        '#type' => 'value',
        '#value' => $handler_id,
      ];

      $element['handler_config'] = ['#process' => [[$this, 'processHandlerConfigurationSubform']]];
      $element['handler_config']['#item'] = $item;
      $element['handler_config']['#handler_id'] = $handler_id;

      // @todo: change elements '#parents' in '#process' callback (it is already set there)
      //   to avoid doing changes to values in massageFormValues()

      // @todo: add validate and submit handlers
    }
    else {
      // @todo: add some fallback behavior though not expected to be used in normal scenarios
    }

    return $element;
  }

  public function processHandlerConfigurationSubform(array $element, FormStateInterface $form_state, $form) {
    $item = $element['#item'];
    $handler_id = $element['#handler_id'];

    $handler_config = $item->getHandlerConfig();
    $plugin = \Drupal::service('plugin.manager.packagist_handler')->createInstance($handler_id, $handler_config);

    // handler plugin buildConfigurationForm() needs SubformState:createForSubform() form_state
    $subform_state = SubformState::createForSubform($element, $form, $form_state);

    // attach handler configuration form
    // @todo: make sure only element_children are appended, i.e. plugin config form
    //   should only add new levels to the sublying level
    $element = $plugin->buildConfigurationForm($element, $subform_state);

    // change handler configuration form container to fieldset if not empty
    if (Element::children($element)) {
      $element['#type'] = 'details';
      $element['#title'] = t('Handler settings');
      $element['#open'] = TRUE;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // @todo: or maybe just set #parents key for handler_config
    foreach ($values as &$value) {
      $handler_config = [];
      if (!empty($value['handler_config'])) {
        foreach ($value['handler_config'] as $handler_config_key => $handler_config_item) {
          $handler_config[$handler_config_key] = $handler_config_item;
        }
        // @todo: unset()
      }
      $value['handler_config'] = serialize($handler_config);
    }
    return $values;
  }

}
