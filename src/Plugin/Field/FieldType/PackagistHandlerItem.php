<?php

namespace Drupal\packagist\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'packagist_handler' field type.
 *
 * @FieldType(
 *   id = "packagist_handler",
 *   label = @Translation("Packagist handler"),
 *   description = @Translation("Packagist handler plugin id and configuration"),
 *   default_widget = "packagist_handler",
 *   default_formatter = "packagist_handler"
 * )
 */
class PackagistHandlerItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['handler_id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Packagist handler id'));
    $properties['handler_config'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Packagist handler config'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        // stores handler plugin id
        'handler_id' => [
          'type' => 'varchar_ascii',
          'description' => 'The ID of the packagist handler plugin.',
          'length' => 255,
        ],
        // stores handler plugin configuration
        'handler_config' => [
          'type' => 'text',
          'size' => 'medium',
          // @todo: check if should be blob
          //'mysql_type' => 'mediumtext',
          //'mysql_type' => 'mediumblob',
          'description' => 'Serialized packagist handler plugin configuration.',
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    // @todo: check for requried constraints

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $handler_id = $this->get('handler_id')->getValue();
    return $handler_id === NULL || $handler_id === '';
  }


  // @todo: use in Packagist::getPackagistHandlerInstance()
  public function getPackagistHandler() {
    // @todo: create instance of the plugin based on id and config from the packagist_handler field
    // @todo: check if not empty
  }

  public function getHandlerConfig() {
    return !empty($this->handler_config) ? unserialize($this->handler_config) : [];
  }

}
