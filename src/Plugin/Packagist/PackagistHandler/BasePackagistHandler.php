<?php

// @todo: review id value and the class name

namespace Drupal\packagist\Plugin\Packagist\PackagistHandler;

use Drupal\packagist\Core\PackagistHandlerBase;

/**
 * Provides a 'Base' Packagist Handler.
 *
 * @PackagistHandler(
 *  id = "base",
 *  label = @Translation("Base")
 * )
 */
class BasePackagistHandler extends PackagistHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function runAction($action, $parameters) {
  }

}
