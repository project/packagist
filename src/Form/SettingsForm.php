<?php

namespace Drupal\packagist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'packagist.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'packagist_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('packagist.settings');
    $form['direct_execution'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Direct execution'),
      // @todo: add a warning into description
      '#description' => $this->t('Execute server-wrapper.sh script directly (not recommended).'),
      '#default_value' => $config->get('direct_execution'),
    ];
    // allow to edit host and port when direct_execution disabled only
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      // @todo: check if domain names are allowed (also check for http/s not included)
      '#description' => $this->t('Backend host name or IP address.'),
      '#default_value' => $config->get('host'),
      '#states' => [
        'required' => [
          ':input[name="direct_execution"]' => ['checked' => FALSE],
        ],
        'disabled' => [
          ':input[name="direct_execution"]' => ['checked' => TRUE],
        ],
      ],
      // The field is only required for indirect execution
      '#element_validate' => ['::validateRequiredElement'],
    ];
    $form['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#description' => $this->t('Backend port number.'),
      '#default_value' => $config->get('port'),
      '#states' => [
        'required' => [
          ':input[name="direct_execution"]' => ['checked' => FALSE],
        ],
        'disabled' => [
          ':input[name="direct_execution"]' => ['checked' => TRUE],
        ],
      ],
      // The field is only required for indirect execution
      '#element_validate' => ['::validateRequiredElement'],
    ];
    // @todo: The value is reset when saved with "Direct execution" enabled.
    //   Browsers do not send disabled elements values as well as they do not
    //   send unchecked checkboxes values. In general case, the backend can't
    //   guess whether the checkbox is disabled or unchecked and just considers
    //   it unchecked.
    // @todo: Use radio button instead of the checkbox.
    $form['enable_ssl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable SSL encryption'),
      '#description' => $this->t('Enable ssl encryiption for communication between client and server hosts (if enabled, must be supported on backend side).'),
      '#default_value' => $config->get('enable_ssl'),
      '#states' => [
        'disabled' => [
          ':input[name="direct_execution"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['auth_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authentication key'),
      '#description' => $this->t('Authentication key for the backend.'),
      '#default_value' => $config->get('auth_key'),
      '#states' => [
        'required' => [
          ':input[name="direct_execution"]' => ['checked' => FALSE],
        ],
        'disabled' => [
          ':input[name="direct_execution"]' => ['checked' => TRUE],
        ],
      ],
      // The field is only required for indirect execution
      '#element_validate' => ['::validateRequiredElement'],
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Do not override the values saved for indirect execution when swiching to
    // direct execution.
    // @todo: see the note on "enable_ssl" checkbox above
    if ($form_state->getValue('direct_execution')) {
      $this->config('packagist.settings')
        ->set('direct_execution', $form_state->getValue('direct_execution'))
        ->save();
    }
    else {
      $this->config('packagist.settings')
        ->set('direct_execution', $form_state->getValue('direct_execution'))
        ->set('host', $form_state->getValue('host'))
        ->set('port', $form_state->getValue('port'))
        ->set('enable_ssl', $form_state->getValue('enable_ssl'))
        ->set('auth_key', $form_state->getValue('auth_key'))
        ->save();
    }
  }


  /**
   * Validate a conditionally required field.
   */
  public function validateRequiredElement(array &$element, FormStateInterface $form_state) {
    if (!$form_state->getValue('direct_execution')) {
      if ('' == trim($form_state->getValue($element['#parents']))) {
        if (isset($element['#title'])) {
          $form_state->setError($element, $this->t('@name field is required.', ['@name' => $element['#title']]));
        }
        else {
          $form_state->setError($element);
        }
      }
    }
  }

}
