<?php

namespace Drupal\packagist\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PackagistTypeForm.
 */
class PackagistTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $packagist_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $packagist_type->label(),
      '#description' => $this->t("Label for the Packagist type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $packagist_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\packagist\Entity\PackagistType::load',
      ],
      '#disabled' => !$packagist_type->isNew(),
    ];

    // @todo: also this should not be changeable for in-config types, e.g. Basic and Git
    // Disalbe packagist handler select if entities exist for the packagist type.
    $handler_select_enabled = FALSE;
    if ($packagist_type->isNew()) {
      $handler_select_enabled = TRUE;
    }
    else {
      $count = \Drupal::entityQuery('packagist')
         ->condition('type', $packagist_type->id())
         ->count()
         ->execute();
      if (!$count) {
        $handler_select_enabled = TRUE;
      }
    }

    // @todo: init service on create
    $options = [];
    $handlers = \Drupal::service('plugin.manager.packagist_handler')->getDefinitions();
    foreach ($handlers as $handler_id => $definition) {
      $options[$handler_id] = $definition['label'];
    }
    $default_handler_id = $packagist_type->isNew() ? '' : $packagist_type->getPackagistHandlerId();
    $form['packagist_handler_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Packagist handler'),
      '#options' => $options,
      '#default_value' => $default_handler_id,
      '#disabled' => !$handler_select_enabled,
      '#empty_value' => '',
      '#empty_option' => t('- Select -'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $packagist_type = $this->entity;
    $status = $packagist_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Packagist type.', [
          '%label' => $packagist_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Packagist type.', [
          '%label' => $packagist_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($packagist_type->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $packagist_handler_id = $form_state->getValue('packagist_handler_id', '');
    $this->entity->set('packagist_handler_id', $packagist_handler_id);
  }

}
