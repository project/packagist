<?php

namespace Drupal\packagist\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Packagist entities.
 *
 * @ingroup packagist
 */
class PackagistDeleteForm extends ContentEntityDeleteForm {


}
