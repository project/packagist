<?php

namespace Drupal\packagist\Backend;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * Provides PackagistBackend wrapper service for bash client script.
 */
class PackagistBackend implements PackagistBackendInterface {

  const API_VERSION = '0.1';

  /**
   * Constructs a new PackagistBackend object.
   */
  public function __construct() {

  }

  protected function getClientScriptPath() {
    $client_script_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'packagist') . '/scripts/client-wrapper.sh';

    return $client_script_path;
  }

  // @todo: needed only for '--direct-execution' case
  protected function getServerScriptPath() {
    $server_script_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'packagist') . '/scripts/server-wrapper.sh';

    return $server_script_path;
  }

  protected function getDirectExecutionSetting() {
    // get direct_execution setting value
    $direct_execution = \Drupal::config('packagist.settings')->get('direct_execution');

    return $direct_execution;
  }

  protected function getHostSetting() {
    // get host setting value
    $host = \Drupal::config('packagist.settings')->get('host');

    return $host;
  }

  protected function getPortSetting() {
    // get port setting value
    $port = \Drupal::config('packagist.settings')->get('port');

    return $port;
  }

  protected function getEnableSslSetting() {
    // get enable_ssl setting value
    $enable_ssl = \Drupal::config('packagist.settings')->get('enable_ssl');

    return $enable_ssl;
  }

  protected function getAuthKeySetting() {
    // get port setting value
    $auth_key = \Drupal::config('packagist.settings')->get('auth_key');

    return $auth_key;
  }

  protected function validateActionArgs($action, $action_args) {
    $errors = [];

    return $errors;
  }

  public function runAction($action, $action_args, $stdin_input = NULL, $explode_output = NULL, $connection_params = []) {
    // make it easier to skip parameter value when calling the method from outside
    if (NULL === $explode_output) {
      $explode_output = TRUE;
    }
    // @todo: by convention, if $connection_params are set, global $direct_execution option
    //   is disabled and the params are used for client-server connection
    if (!empty($connection_params)) {
      // the value is still needed for runClientScript() call bellow
      $direct_execution = FALSE;
      // @todo: make sure all required parameters are set
    }
    else {
      $direct_execution = $this->getDirectExecutionSetting();
      if (!$direct_execution) {
        // @todo: check that host and port are not empty, emit an error otherwise
        $host = $this->getHostSetting();
        $port = $this->getPortSetting();
        $enable_ssl = $this->getEnableSslSetting();
        $auth_key = $this->getAuthKeySetting();

        // @todo: maybe rename to client_script_params..
        //   also maybe split params into groups: client script and server script params
        $connection_params = [
          'host' => $host,
          'port' => $port,
          'enable_ssl' => $enable_ssl,
          'auth_key' => $auth_key,
        ];
      }
    }

    $allowed_actions = [
      // @todo:
      'create_json_file',
      'git_clone',
      'git_fetch',
      'git_list_tags',
      'git_get_status',
      'git_update',
      'get_server_script_api_version',
      'generate_archive',
      'composer_json_by_commit_ref',
      'put_archive_file',
      //'get_remote_server_script_api_version',
      //'get_remote_api_version',
    ];

    if (!in_array($action, $allowed_actions)) {
      $status = [
        // @todo: set action name
        'status' => '0',
        'message' => t('Action @action is not allowed', ['@action' => $action]),
        'result' => [
          'data' =>  [],
        ],
      ];
    }
    elseif ($errors = $this->validateActionArgs($action, $action_args)) {
      // @todo: validate args, return corresponding status info in case errors found
      // @todo: return $status info with error message or emit an error
      //   maybe also add an entry to the log
      // @todo: maybe use $this->errors array property
    }
    else {
      $script_params = [
        '--action', $action,
      ];

      $action_params = [];

      // @todo: prepare params based on $action and $action_args
      //   or just add '--' prefix to args names for now
      foreach ($action_args as $arg_key => $arg_value) {
        $param_key = '--' . str_replace('_', '-', $arg_key);
        $action_params[] = $param_key;
        $action_params[] = $arg_value;
      }

      // avoid overriding '--action' parameter
      //$script_params += $action_params;
      $script_params = array_merge($script_params, $action_params);

      // @todo: array of output strings
      $result = $this->runClientScript($script_params, $stdin_input, $direct_execution, $explode_output, $connection_params);
      // @todo: use constants for status codes
      if ($result['status'] == 'ok') {
        $status = '1';
        $message = 'ok';
        $data = $result['output'];
      }
      else {
        $status = 'error';
        $message = 'error';
        // @todo: maybe also add output for debug mode or write into log
        //   though all info should be in error message text
        $data = [];
      }

      // @todo: if needed, add additional info, e.g. script_params, for debug mode
      $status_info = [
        'status' => $status,
        'message' => $message,
        'data' =>  $data,
      ];
    }

    return $status_info;
  }

  public function runClientScript($script_params, $stdin_input = NULL, $direct_execution = FALSE, $explode_output = TRUE, $connection_params = []) {
    $client_script_path = $this->getClientScriptPath();

    if (!is_array($script_params)) {
      // @todo
    }

    // add '--direct-execution' parameter to the $script_params list
    if ($direct_execution) {
      $script_params[] = '--direct-execution';

      $script_params[] = '--server-wrapper-script';
      $script_params[] = $this->getServerScriptPath();
    }
    else {
      // add connection_params to the $script_params list
      $script_params = array_merge($script_params, [
        '--host', $connection_params['host'],
        '--port', $connection_params['port'],
        // while port and host are used by the client to connect to the server,
        //   auth_key is sent to the server to be checked against the key stored on it
        '--auth-key', $connection_params['auth_key'],
      ]);
      // add '--enable-ssl' parameter to the $script_params list
      if ($connection_params['enable_ssl']) {
        $script_params[] = '--enable-ssl';
      }
    }

    // always check api version even though it may be not needed for direct_execution case
    $script_params[] = '--api-version';
    $script_params[] = static::API_VERSION;

    $process = new Process(array_merge(['/bin/bash', $client_script_path], $script_params));
    //$process = new Process(array_merge([$client_script_path], $script_params));

    // @todo: is it needed if $stdin_input is emtpy? or NULL?
    //   i.e. do not add if NULL but add if and empty string
    $process->setInput($stdin_input);
    $process->run();

    // @todo: check success status
    //   maybe check exit code value if possible if not successful
    // executes after the command finishes
    if (!$process->isSuccessful()) {
      // @todo: log error message (see Process() methods) but do not show it to the client
      throw new ProcessFailedException($process);
    }

    // @todo: get command execution output
    //   check for best practeces and examples for output processing
    $command_output = $process->getOutput();

    // @todo: check for errors and set corresponding status code
    //   use constants for status codes
    $status = [
      'status' => 'ok',
    ];
    if ($explode_output) {
      $status['output'] = array_filter(explode("\n", $command_output));
    }
    else {
      $status['output'] = $command_output;
    }

    return $status;
  }

}
