<?php

namespace Drupal\packagist\Generator;

use Drupal\packagist\Entity\PackageJsonSha;

/**
 * Packagist json structure generator service.
 *
 * Generates json files for packagist entity to build a complete
 * and ready to use packagist structure.
 */
class PackagistGenerator implements PackagistGeneratorInterface {

  // @todo: review structure types names
  const DEFAULT_STRUCTURE_TYPE = 'providers_default';

  const BACKEND_ACTION_CREATE_JSON_FILE = 'create_json_file';

  protected $backend_client_wrapper;

  /**
   * Constructs a new PackagistGenerator object.
   */
  public function __construct() {
    $this->backend_client_wrapper = \Drupal::service('packagist.backend.client.wrapper');
  }

  // @todo: review methods names

  // @todo: the service itself knows nothing about packagist entities


  /**
   * Regenerate packagist structure.
   */
  public function regeneratePackagistStructure($packages, $base_path, $base_url, $packagist_id, $structure_type_settings = NULL) {
    // @todo: Implement the logic
    // There may be two options implemented:
    // - use database data from PackageJsonSha entries only
    // - regenerate completely by packagist handler from scratch - in this case
    //   packagist handler would need to implement a method for that.
  }


  // @todo: maybe add $structure_type to arguments
  // @todo: different structure types could be implemented by corresponding plugins,
  //   would allow to make it extensible
  // @todo: should be implemented using queues since there may be multiple
  //   packages to generate if the whole packagist structure is regenerated
  // @todo: although the service doesn't use packagist entities directly
  //   and is not supposed to know anything about packagist entities, we still
  //   use packagist_id for referecing these entities in PackageHashSha records
  // @todo: rename the first argument to $updated_packages
  //   though also should consider that these are new packages

  /**
   * Update packagist structure.
   *
   * Update or create required packagist json files related to the given packages.
   */
  public function updatePackagistStructure($packages, $base_path, $base_url, $packagist_id, $structure_type_settings = NULL, $connection_params = []) {
    if (NULL == $structure_type_settings) {
      $structure_type_settings = [
        'type' => static::DEFAULT_STRUCTURE_TYPE,
        'packages_subdir' => 'packages',
        'providers_key' => 'providers-url',
      ];
    }

    $action = static::BACKEND_ACTION_CREATE_JSON_FILE;

    // @todo: remove outdated files, i.e. older versions of package schema json
    //   and provider packages json files

    // @todo: generate structure based on structure type
    if ($structure_type_settings['type'] == static::DEFAULT_STRUCTURE_TYPE) {
      $vendor_hashes = [];
      foreach ($packages as $vendor_name => $projects) {
        foreach ($projects as $project_name => $tag_packages) {
          // @todo: since the hash is supposed to be stored in any case and the entity loaded,
          //   first load it here to obtain extra_info with uids
          // @todo: see https://github.com/composer/composer/issues/2738
          // @todo:
          $entity = $this->getPackageHashEntity($vendor_name, $project_name, $packagist_id);
          if ($entity) {
            $extra_info = $entity->getExtraInfo();
          }
          else {
            // @todo: review the format and structure, add description
            $extra_info = [
              'tags_uids' => [
              ],
            ];
          }

          // @todo: setting max_uid should be done in a blocking manner,
          //   i.e. if two requests are made simultaneously, the same new uid
          //   value should not be set for two entries to avoid conflicts
          //   also maybe use some kind of transactions, in case errors appear before
          //   values are written to the database, the new uid value would
          //   get occupied but not actually used


          // @todo: the uids for each tag should be also stored along with other data
          //   in the tables and shouldn't change (?) when additional tags are added
          //   and content regenerated
          //   Also, it should be stored here since it is about structure type
          //   but not about repository itself, i.e. not in one of its fields properties
          //   e.g. packages_config.


          // @todo: use (and store) extra_info data for uid mappings
          //   (though should it really be unchangeble?)
          // @todo: get max_uid only when needed
          // @todo: lock the value until finished to avoid conflicts with possible concurrent
          //   requests (see the comment above)
          $max_uid = $this->getMaxUid($packagist_id);
          foreach ($tag_packages as $k => $tag_package) {
            if (isset($extra_info['tags_uids'][$k])) {
              $uid = $extra_info['tags_uids'][$k];
            }
            else {
              $max_uid++;
              $uid = $max_uid;
              $extra_info['tags_uids'][$k] = $uid;
            }

            // @todo: check if $k contains version in required format,
            //   move the check into a method
            // @todo: make sure that 'v' is added to tags where needed to make
            //   correct versions notations
            //   see https://stackoverflow.com/questions/39974833/composer-the-requested-package-exists-as-but-these-are-rejected-by-your-constr
            //   filter tags names by pattern to allow only valid versions tags
            // @todo: also this will use uid from composer original schema arr if set
            //   though should not
            $version = $k;
            if (strpos($k, 'v') !== 0) {
              // Add 'v' prefix to tag names to match composer versions format
              $version = "v{$version}";
            }
            // @todo: add the keys to the beginning of json info array
            $tag_packages[$k] = array_merge(['uid' => $uid, 'version' => $version], $tag_package);
          }

          $package_full_name = "{$vendor_name}/{$project_name}";
          $json_arr = [
            'packages' => [
              $package_full_name => $tag_packages,
            ],
          ];
          $package_json = json_encode($json_arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

          // create/update package_package_json_sha entity instance
          $package_hash = hash("sha256", $package_json);
          $sha = $package_hash;
          $this->storePackageHash($vendor_name, $project_name, $sha, $package_json, $packagist_id, $extra_info, $max_uid);

          // generate package schema json file
          $parameters = [
            'file_dir_path' => $base_path . "/p/packages/{$vendor_name}/{$project_name}",
            'file_name' => "{$sha}.json",
          ];
          $status_info = $this->backend_client_wrapper->runAction($action, $parameters, $package_json, NULL, $connection_params);
        }
      }

      // get all the package_hashes from the database
      // @todo: include only 'published' or 'enabled' (when implemented)
      $result = \Drupal::database()->select('packagist_package_json_sha', 'f')
        ->fields('f', ['vendor_name', 'project_name', 'sha'])
        ->condition('packagist_id', $packagist_id)
        // @todo: add conditions for 'not empty' for vendor_name and project_name
        //   though should be set on database level and should not be allowed at all
        ->condition('vendor_name', '', '!=')
        ->condition('project_name', '', '!=')
        ->execute();

      $package_hashes = [];
      while ($item = $result->fetchAssoc()) {
        $package_hashes["{$item['vendor_name']}/{$item['project_name']}"] = [
          'sha256' => $item['sha']
        ];
      }

      // @todo: the part below may be common with regeneratePackagistStructure() method
      // genereate provider packages hashes list json file
      $json_arr = [
        'providers' => $package_hashes,
      ];
      $package_json = json_encode($json_arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
      $file_hash = hash("sha256", $package_json);
      $parameters = [
        'file_dir_path' =>  "{$base_path}/p/provider-latest",
        'file_name' => "{$file_hash}.json",
      ];
      $status_info = $this->backend_client_wrapper->runAction($action, $parameters, $package_json, NULL, $connection_params);

      // generate packages.json file
      $json_arr = [
        // @todo: use base_url (and rename the variable since it is not full url,
        //   but the part after domain name)
        // @todo: check if leading slash is required
        // @todo: base_url can be empty if domain root is supposed to be used,
        //   in that case triling slash should not be added
        //   also consider the case when the full url (including domain name) is used
        'providers-url' => '/' . $base_url . '/p/packages/%package%/%hash%.json',
        'provider-includes' => [
          'p/provider-latest/%hash%.json' => [
            'sha256' => $file_hash,
          ],
        ],
      ];
      $package_json = json_encode($json_arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
      $parameters = [
        'file_dir_path' => $base_path,
        'file_name' => "packages.json",
      ];
      $status_info = $this->backend_client_wrapper->runAction($action, $parameters, $package_json, NULL, $connection_params);

      return $status_info;
    }


  }

  // @todo: set vendor_name, project_name, packagist_id as a primary key for the entity
  protected function getPackageHashEntity($vendor_name, $project_name, $packagist_id) {
    $query = \Drupal::entityQuery('packagist_package_json_sha');
    $query->condition('vendor_name', $vendor_name);
    $query->condition('project_name', $project_name);
    $query->condition('packagist_id', $packagist_id);
    $ids = $query->execute();

    if (!empty($ids)) {
      $package_hash_entity = PackageJsonSha::load(reset($ids));
    }
    else {
      $package_hash_entity = NULL;
    }

    return $package_hash_entity;
  }

  /**
   * Get maximum uid value for PackageJsonSha max_uid field.
   */
  protected function getMaxUid($packagist_id) {
    // set the default max value
    $max_uid = 1000000;
    // @todo: maybe use aggregate max() entity query instead,
    //   see https://www.drupal.org/node/1918702
    $query = \Drupal::database()->select('packagist_package_json_sha', 'p');
    $query->condition('p.packagist_id', $packagist_id);
    // consider empty table case,
    // see https://stackoverflow.com/questions/15475059/how-to-treat-max-of-an-empty-table-as-0-instead-of-null
    $query->addExpression('COALESCE(MAX(p.max_uid), 0)', 'max');
    $result = $query->execute()->fetchAssoc();
    $max_uid = max($max_uid, $result['max']);

    return $max_uid;
  }

  /**
   * Store a new PackageJsonSha entity or update existing one.
   */
  protected function storePackageHash($vendor_name, $project_name, $sha, $package_json, $packagist_id, $extra_info, $max_uid) {
    $query = \Drupal::entityQuery('packagist_package_json_sha');
    $query->condition('vendor_name', $vendor_name);
    $query->condition('project_name', $project_name);
    $query->condition('packagist_id', $packagist_id);
    $ids = $query->execute();

    // @todo: only one entity is supposed to be there
    if (!empty($ids)) {
      // @todo: All files with sha hash values that are not present in the given
      //   table are considered outdated and could be removed at garbage collection.
      //   Another approach would be to create a new entity every time instead of
      //   updating existing one, and setting some "active" (or "enabled") flag
      //   to FALSE to distiguish them from latest actual records or to use created
      //   timestamp value and to consider outdated with older values (when field
      //   added to the entity schema).

      $package_hash_entity = PackageJsonSha::load(reset($ids));
      $package_hash_entity->sha = $sha;
      // @todo: package_json is never used for now
      $package_hash_entity->package_json = $package_json;
      $package_hash_entity->setExtraInfo($extra_info);
      $package_hash_entity->max_uid = $max_uid;
      $package_hash_entity->save();
    }
    else {
      PackageJsonSha::create([
        'vendor_name' => $vendor_name,
        'project_name' => $project_name,
        'sha' => $sha,
        'packagist_id' => $packagist_id,
        // @todo: package_json is never used for now
        'package_json' => $package_json,
        'max_uid' => $max_uid,
      ])
      ->setExtraInfo($extra_info)
      ->save();
    }
  }

}
