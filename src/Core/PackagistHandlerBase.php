<?php

namespace Drupal\packagist\Core;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for Packagist handler plugins.
 */
abstract class PackagistHandlerBase extends PluginBase implements PackagistHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'base_path' => '',
      // @todo: mention in description that this stands for suburl
      // @todo: The setting may be not needed for some types of packagist structure,
      //   namely the most basic one when all packages with their data are listed
      //   in a single packagist.json file - the suburl is not needed there since
      //   there are no other files involved
      //   For that reason, maybe move into an extending class.
      // @todo: for the value maybe do not use the leading slash, i.e. trim it
      'base_url' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    // @todo: use NestedArray::mergeDeep here. See BlockBase::setConfiguration for example.
    // @todo: also do the same for all other plugin types
    $this->configuration = $configuration + $this->defaultConfiguration();
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Clean submitted values
    //$handler_config = $this->extractFormValues($form, $form_state);
    //$form_state->setValues($drawer_config);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['base_path'] = [
      '#type' => 'textfield',
      '#title' => t('Base path'),
      '#default_value' => $this->configuration['base_path'],
      '#required' => TRUE,
    ];
    $form['base_url'] = [
      '#type' => 'textfield',
      //'#type' => 'url',
      '#title' => t('Base url'),
      '#default_value' => $this->configuration['base_url'],
      '#description' => t('Use "/" sign when the root path is used'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function runAction($action, $parameters);

  //public function implementsAction($action) {
  //}

}
