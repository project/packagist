<?php

namespace Drupal\packagist\Core;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for Packagist handler plugins.
 */
interface PackagistHandlerInterface extends PluginInspectionInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Run handler action.
   */
  public function runAction($action, $parameters);

}
