<?php

namespace Drupal\packagist\Manager;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Packagist handler plugin manager.
 */
class PackagistHandlerManager extends DefaultPluginManager {


  /**
   * Constructs a new PackagistHandlerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Packagist/PackagistHandler', $namespaces, $module_handler, 'Drupal\packagist\Core\PackagistHandlerInterface', 'Drupal\packagist\Annotation\PackagistHandler');

    $this->alterInfo('packagist_handler_info');
    $this->setCacheBackend($cache_backend, 'packagist_handler_plugins');
  }

}
