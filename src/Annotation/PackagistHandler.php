<?php

namespace Drupal\packagist\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Packagist handler item annotation object.
 *
 * @see \Drupal\packagist\Plugin\PackagistHandlerManager
 * @see plugin_api
 *
 * @Annotation
 */
class PackagistHandler extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
