<?php

namespace Drupal\packagist\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Packagist entity.
 *
 * @ingroup packagist
 *
 * @ContentEntityType(
 *   id = "packagist",
 *   label = @Translation("Packagist"),
 *   bundle_label = @Translation("Packagist type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\packagist\PackagistListBuilder",
 *     "views_data" = "Drupal\packagist\Entity\PackagistViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\packagist\Form\PackagistForm",
 *       "add" = "Drupal\packagist\Form\PackagistForm",
 *       "edit" = "Drupal\packagist\Form\PackagistForm",
 *       "delete" = "Drupal\packagist\Form\PackagistDeleteForm",
 *     },
 *     "access" = "Drupal\packagist\PackagistAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\packagist\PackagistHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "packagist",
 *   admin_permission = "administer packagist entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/packagist/packagist/{packagist}",
 *     "add-page" = "/admin/packagist/packagist/add",
 *     "add-form" = "/admin/packagist/packagist/add/{packagist_type}",
 *     "edit-form" = "/admin/packagist/packagist/{packagist}/edit",
 *     "delete-form" = "/admin/packagist/packagist/{packagist}/delete",
 *     "collection" = "/admin/packagist/packagist",
 *   },
 *   bundle_entity_type = "packagist_type",
 *   field_ui_base_route = "entity.packagist_type.edit_form"
 * )
 */
class Packagist extends ContentEntityBase implements PackagistInterface {

  use EntityChangedTrait;

  const PACKAGIST_HANDLER_FIELD = 'packagist_handler';

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Packagist entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Packagist entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Packagist is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    // @todo: add packagist_handler field widget and formatter settings
    $fields[static::PACKAGIST_HANDLER_FIELD] = BaseFieldDefinition::create('packagist_handler')
      ->setLabel(t('Packagist Handler'))
      ->setDescription(t('The packagist handler plugin id and configuration.'))
      ->setSettings([
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'packagist_handler',
        //'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'packagist_handler',
        //'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    return $fields;
  }

  /**
   * Get packagist_handler plugin instance based on entity field value.
   *
   * Packagist entity knows nothing about the actions implemented
   * by handler plugins, thus just allow to get the plugin instance itself
   * where needed and execute required actions directly.
   * By convention, Packagist entity itself doesn't implement any actions
   * at all - everything is done by corresponding handler plugins.
   */
  public function getPackagistHandlerInstance() {
    $packagist_handler_item = $this->{static::PACKAGIST_HANDLER_FIELD}->get(0);
    if ($packagist_handler_item) {
      $handler_id = $packagist_handler_item->handler_id;
      $handler_config = $packagist_handler_item->getHandlerConfig();

      // @todo: set service in constructor
      $plugin = \Drupal::service('plugin.manager.packagist_handler')->createInstance($handler_id, $handler_config);

      return $plugin;
    }
    else {
      // @todo:
      return FALSE;
    }
  }

}
