<?php

namespace Drupal\packagist\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the PackageJsonSha entity.
 *
 * Used to store packages sha hashes along with other data. Can be used
 * to manage outdated records, e.g. when a package json gets changed.
 *
 * @ContentEntityType(
 *   id = "packagist_package_json_sha",
 *   label = @Translation("Package Json (sha)"),
 *   base_table = "packagist_package_json_sha",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class PackageJsonSha extends ContentEntityBase implements ContentEntityInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // @todo: check if any fields should be set to not null explicitly
    $fields['vendor_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Vendor name'))
      ->setDescription(t('The name of the vendor.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 64,
      ])
      ->setRequired(TRUE);
    $fields['project_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project name'))
      ->setDescription(t('The name of the project.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 64,
      ])
      ->setRequired(TRUE);
    $fields['sha'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Library name'))
      ->setDescription(t('The name of the library package.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 128,
      ])
      ->setRequired(TRUE);
    $fields['packagist_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Packagist'))
      ->setDescription(t('The packagist ID'))
      ->setSetting('target_type', 'packagist')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE);

    // @todo: package_json is not used anywhere for now (may be used to regenerate packages.json
    //   for the packages without collecting and structuring all the info and addressing source
    //   providers logics and submodules)
    // @todo: can also later be used to explore package version composer data without
    //   addressing git repository
    $fields['package_json'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Package JSON'))
      ->setDescription(t('The package json to be added to packages.json structure.'))
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['extra_info'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Extra info'))
      ->setDescription(t('The extra info used to generate packagist structure, may depend on structure type.'))
      ->setDefaultValue('')
      ->setRequired(FALSE);

    // @todo: should it be numeric or is any string allowed for package version info uid values?
    $fields['max_uid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Max UID'))
      ->setReadOnly(TRUE)
      // @todo: set default value to '0' if needed (e.g. for comparisons)
      ->setSetting('unsigned', TRUE);

    // @todo: store package entity id in extra_info for those handlers using entities
    //   for stroing packages info along with any other required additional data
    // @todo: a numeric field could be added to store entity reference id
    //   i.e. without limiting to any entity type, also there potentially may be packagist
    //   hanlders that do not use any entities for storing packages info

    // @todo: add 'enabled' flag field to be used when packagist json
    //   structure needs to be regenerated, i.e. to define which records should be included

    // @todo: add created timestamp column

    return $fields;
  }

  // @todo: add getter and setter methods

  /**
   * Get extra_info value.
   */
  public function getExtraInfo() {
    $extra_info = !empty($this->extra_info->get(0)->value)
      ? unserialize($this->extra_info->get(0)->value) : [];
    return $extra_info;
  }

  /**
   * Set extra_info value.
   */
  public function setExtraInfo($extra_info) {
    $this->extra_info = serialize($extra_info);
    return $this;
  }

}

