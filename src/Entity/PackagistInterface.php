<?php

namespace Drupal\packagist\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Packagist entities.
 *
 * @ingroup packagist
 */
interface PackagistInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Packagist name.
   *
   * @return string
   *   Name of the Packagist.
   */
  public function getName();

  /**
   * Sets the Packagist name.
   *
   * @param string $name
   *   The Packagist name.
   *
   * @return \Drupal\packagist\Entity\PackagistInterface
   *   The called Packagist entity.
   */
  public function setName($name);

  /**
   * Gets the Packagist creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Packagist.
   */
  public function getCreatedTime();

  /**
   * Sets the Packagist creation timestamp.
   *
   * @param int $timestamp
   *   The Packagist creation timestamp.
   *
   * @return \Drupal\packagist\Entity\PackagistInterface
   *   The called Packagist entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Packagist published status indicator.
   *
   * Unpublished Packagist are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Packagist is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Packagist.
   *
   * @param bool $published
   *   TRUE to set this Packagist to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\packagist\Entity\PackagistInterface
   *   The called Packagist entity.
   */
  public function setPublished($published);

}
