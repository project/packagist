<?php

namespace Drupal\packagist\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Packagist type entity.
 *
 * @ConfigEntityType(
 *   id = "packagist_type",
 *   label = @Translation("Packagist types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\packagist\PackagistTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\packagist\Form\PackagistTypeForm",
 *       "edit" = "Drupal\packagist\Form\PackagistTypeForm",
 *       "delete" = "Drupal\packagist\Form\PackagistTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\packagist\PackagistTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "packagist_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "packagist",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "packagist_handler_id"
 *   },
 *   links = {
 *     "canonical" = "/admin/packagist/packagist-types/{packagist_type}",
 *     "add-form" = "/admin/packagist/packagist-types/add",
 *     "edit-form" = "/admin/packagist/packagist-types/{packagist_type}/edit",
 *     "delete-form" = "/admin/packagist/packagist-types/{packagist_type}/delete",
 *     "collection" = "/admin/packagist/packagist-types"
 *   }
 * )
 */
class PackagistType extends ConfigEntityBundleBase implements PackagistTypeInterface {

  /**
   * The Packagist type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Packagist type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Packagist handler plugin ID.
   *
   * @var string
   */
  protected $packagist_handler_id;

  /**
   * {@inheritdoc}
   *
   * @todo: add to interface
   */
  public function getPackagistHandlerId() {
    return $this->packagist_handler_id;
  }
}
