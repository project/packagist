<?php

namespace Drupal\packagist\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Packagist type entities.
 */
interface PackagistTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
