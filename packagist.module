<?php

/**
 * @file
 * Contains packagist.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function packagist_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the packagist module.
    case 'help.page.packagist':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Enables packagist services client and server.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function packagist_theme() {
  $theme = [];
  $theme['packagist'] = [
    'render element' => 'elements',
    'file' => 'packagist.page.inc',
    'template' => 'packagist',
  ];
  $theme['packagist_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'packagist.page.inc',
  ];
  return $theme;
}

/**
* Implements hook_theme_suggestions_HOOK().
*/
function packagist_theme_suggestions_packagist(array $variables) {
  $suggestions = [];
  $entity = $variables['elements']['#packagist'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'packagist__' . $sanitized_view_mode;
  $suggestions[] = 'packagist__' . $entity->bundle();
  $suggestions[] = 'packagist__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'packagist__' . $entity->id();
  $suggestions[] = 'packagist__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_menu_links_discovered_alter().
 */
function packagist_menu_links_discovered_alter(&$links) {
  $entityTypeManager = \Drupal::entityTypeManager();

  // Add packagist links for each packagist type.
  foreach ($entityTypeManager->getStorage('packagist_type')->loadMultiple() as $type) {
    $links['packagist.add.' . $type->id()] = [
      'title' => t($type->label()),
      'provider' => 'packagist',
      'route_name' => 'entity.packagist.add_form',
      'parent' => 'entity.packagist.add_page',
      'route_parameters' => ['packagist_type' => $type->id()],
    ];
  }
}
