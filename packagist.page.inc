<?php

/**
 * @file
 * Contains packagist.page.inc.
 *
 * Page callback for Packagist entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Packagist templates.
 *
 * Default template: packagist.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_packagist(array &$variables) {
  // Fetch Packagist Entity Object.
  $packagist = $variables['elements']['#packagist'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
