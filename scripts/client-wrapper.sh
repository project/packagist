#!/bin/bash

# @todo: description

# @todo: list all allowed options keys for server scripts here to avoid conflicts

# @todo: set port and host as script arguments
# @todo: set direct_execution flag as script argument (choose a better name)

HOST=""
PORT=""
ENABLE_SSL=""
# @todo: check if '--direct-execution' flag is set (no value required)
# WARNING: it is not safe to enable this option
DIRECT_EXECUTION=false
# the script path should be only set with '--direct-execution' set to 'true'
# @todo: use only one of them, seems to better use server-wrapper script (for consistency and possible changes to sever-core signature)
SERVER_CORE_SCRIPT_PATH=""
SERVER_WRAPPER_SCRIPT_PATH=""

AUTH_KEY=""
ACTION=""

CLIENT_STDIN="$(cat -)"
CLIENT_ARGUMENTS="$*"
SERVER_STDIN=""


#SERVER_STDIN="$CLIENT_ARGUMENTS
#$CLIENT_STDIN"

SERVER_STDIN="$(printf "%s\n%s\n" "$CLIENT_ARGUMENTS" "$CLIENT_STDIN")"

# @todo: set options values from script arguments

# As long as there is at least one more argument, keep looping
while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        -a|--action)
        shift
        ACTION="$1"
        ;;

        # @todo: this may be needed only for debug mode, auth_key is checked on the server side
        #-k|--auth-key)
        #shift
        #AUTH_KEY="$1"
        #;;

        # WARNING: it is not safe to enable this option
        -e|--direct-execution)
        # this is just used as a flag, without value
        #shift
        DIRECT_EXECUTION=true
        ;;

        -w|--server-wrapper-script)
        shift
        SERVER_WRAPPER_SCRIPT_PATH="$1"
        ;;

        # @todo: remove all short versions for option names including server script as well
        --host)
        shift
        HOST="$1"
        ;;

        --port)
        shift
        PORT="$1"
        ;;

        --enable-ssl)
        # this is just used as a flag, without value
        #shift
        ENABLE_SSL=true
        ;;

        *)
        # Do whatever you want with extra options
        # @todo: only echo in debug mode or to log file
        #echo "Unknown option '$key'"
        # @todo: emit an error
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

# @todo: remove '--direct-execution' argument if not needed in server-wrapper.sh
#   though may be needed for additional security measures

# @todo: only echo in debug mode or to log file
#echo AUTH_KEY: "$AUTH_KEY"
#echo DIRECT_EXECUTION: "$DIRECT_EXECUTION"
#echo ACTION: "$ACTION"
#echo SERVER_WRAPPER_SCRIPT_PATH: "$SERVER_WRAPPER_SCRIPT_PATH"

#echo CLIENT_ARGUMENTS: "$CLIENT_ARGUMENTS"
#echo CLIENT_STDIN:
#echo "$CLIENT_STDIN"
#echo SERVER_STDIN:
#echo "$SERVER_STDIN"

# @todo: prepare stdin for server-wrapper.sh (also include arguments as first string
#   for both, direct and indirect execution)

# @todo: see https://stackoverflow.com/questions/2953646/how-can-i-declare-and-use-boolean-variables-in-a-shell-script
if [ "$DIRECT_EXECUTION" = true ]; then
  # @todo: only echo in debug mode or to log file
  #echo ""
  #echo direct execution
  #cd to the current script directory
  #echo pwd: "$(pwd)"
  #echo dirname: "$(dirname "$0")"

  printf '%s' "$SERVER_STDIN" | /bin/bash "$SERVER_WRAPPER_SCRIPT_PATH"
else
  # @todo: check if host and port are set and available
  # use encrypted connection for remote hosts
  if [ "$ENABLE_SSL" = true ]; then
    # @todo: check if '-v' option is required here
    # @todo: check that host and port are set and valid, that host is accessible
    printf '%s' "$SERVER_STDIN" | ncat "$HOST" "$PORT" --ssl -v
  else
    printf '%s' "$SERVER_STDIN" | ncat "$HOST" "$PORT"
  fi
fi



