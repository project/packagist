#!/bin/bash

# check --api-version
LOCAL_API_VERSION="0.1"
API_VERSION=""

# @todo: description

# @todo: add argument for debug mode, output additional info

# @todo: only echo in debug mode or to log file
#echo ""
#echo ===== SERVER CORE execution =====
#echo ""

ACTION=""
SERVER_CORE_PARAMETERS="$*"
SERVER_CORE_STDIN="$(cat -)"
GIT_REPOS_PATH=""
REPOSITORY_DIR_NAME=""
REPOSITORY_URL=""
ARCHIVES_PATH=""
PACKAGE_ARCHIVES_DIR_NAME=""
TAG_NAME=""
FILE_DIR_PATH=""
FILE_NAME=""
ARCHIVE_FILE_NAME=""
AUTH_KEY=""
LOCAL_AUTH_KEY=""

ALLOW_PUT_ARCHIVE=false

# As long as there is at least one more argument, keep looping
while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
      -a|--action)
        shift
        ACTION="$1"
        ;;
      -g|--git-repos-path)
        shift
        GIT_REPOS_PATH="$1"
        ;;
      -n|--repository-dir-name)
        shift
        REPOSITORY_DIR_NAME="$1"
        ;;
      -u|--repository-url)
        shift
        REPOSITORY_URL="$1"
        ;;
      -h|--archives-path)
        shift
        ARCHIVES_PATH="$1"
        ;;
      -p|--package-archives-dir-name)
        shift
        PACKAGE_ARCHIVES_DIR_NAME="$1"
        ;;
      -t|--tag-name)
        shift
        TAG_NAME="$1"
        ;;
      -z|--archive-file-name)
        shift
        ARCHIVE_FILE_NAME="$1"
        ;;

      # @todo: maybe rename to --file-path or rename --archives-path to --archives-dir-path
      #   though this doesn't suppose dir name whereas archives do
      -d|--file-dir-path)
        shift
        FILE_DIR_PATH="$1"
        ;;
      -f|--file-name)
        shift
        FILE_NAME="$1"
        ;;

      --auth-key)
        shift
        AUTH_KEY="$1"
        ;;
      --local-auth-key)
        shift
        LOCAL_AUTH_KEY="$1"
        ;;

      --api-version)
        shift
        API_VERSION="$1"
        ;;

      --allow-put-archive)
        # only parameter name is expected without value, thus shift is not needed here
        ALLOW_PUT_ARCHIVE=true
        ;;

      *)
        # Do whatever you want with extra options
        # @todo: only echo in debug mode or to log file
        #echo "Unknown option '$key'"
        # @todo: emit an error
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

if [ "$AUTH_KEY" != "$LOCAL_AUTH_KEY" ]; then
  # @todo: emit an error
  exit;
fi

if [ "$API_VERSION" != "$LOCAL_API_VERSION" ]; then
  # @todo: emit an error
  exit;
fi

# @todo: review actions names and the list itself
case $ACTION in
  # @todo: reanme to create_and_write_file
  create_json_file )
    # @todo: check if filename has json extension
    # @todo: create directory if needed
    mkdir -p "$FILE_DIR_PATH"
    cd "$FILE_DIR_PATH"
    printf "%s" "$SERVER_CORE_STDIN" > "$FILE_NAME"

    #echo test script action

    ;;

  git_clone )
    # @todo: get git_repos_directory path, repo_url and repo_dir for the package

    # @todo: allow to explicitly disable git functionlity using parameters
    #   e.g. for the case when the packagist_repos module is disabled
    # @todo: check if operation succeeded. e.g. if user has write permissions for the path
    mkdir -p "$GIT_REPOS_PATH"
    cd "$GIT_REPOS_PATH"
    git clone --mirror "$REPOSITORY_URL" "$REPOSITORY_DIR_NAME"
    # @todo: use it suppress errors in case directory exists, review and add additional checks
    if [ $? -eq 0 ]; then
      echo OK
    else
      echo FAIL
      # @todo: anyway we need to know dir name here, e.g. to get tags and generate packages
    fi
    ;;

  git_fetch )
    # @todo: check if directory exists and repository is cloned
    cd "$GIT_REPOS_PATH/$REPOSITORY_DIR_NAME"
    # @todo: do not show any output to the client, check the result here
    git fetch --prune
    ;;

  git_list_tags )
    # @todo: make sure the repository is already cloned and directory exists
    cd "$GIT_REPOS_PATH/$REPOSITORY_DIR_NAME"
    git tag
    ;;
  generate_archive )
    # @todo: also archive type may be set as parameter if '-o' is not used for git archive
    cd "$GIT_REPOS_PATH/$REPOSITORY_DIR_NAME"
    mkdir -p "$ARCHIVES_PATH/$PACKAGE_ARCHIVES_DIR_NAME"
    git archive -o "$ARCHIVES_PATH/$PACKAGE_ARCHIVES_DIR_NAME/$ARCHIVE_FILE_NAME" "$TAG_NAME"
    ;;
  composer_json_by_commit_ref )
    cd "$GIT_REPOS_PATH/$REPOSITORY_DIR_NAME"
    git show $TAG_NAME:composer.json
    ;;

  # @todo: add 'put_archive_file' action, also make it disabled by default,
  #   allow to enable via config (and enable for direct execution via settings)

  # it is similar to 'create_json_file' action
  put_archive_file )
    # check if putting files is allowed, should be restricted by default
    if [ "$ALLOW_PUT_ARCHIVE" = true ] ; then
      # @todo: make sure it works for files without extension, now it uses filename for extension in such a case
      # see https://stackoverflow.com/questions/965053/extract-filename-and-extension-in-bash
      FILE_EXTENSION="${FILE_NAME##*.}"
      # @todo: allow also 'tar' extension
      if [ "$FILE_EXTENSION" = "zip" ] ; then
        # create directory if needed
        mkdir -p "$FILE_DIR_PATH"
        cd "$FILE_DIR_PATH"
        printf "%s" "$SERVER_CORE_STDIN" | base64 --decode > "$FILE_NAME"

        # @todo: else emit an error
      fi
    fi
    ;;

  #test_action )
    #echo this is some action
    #;;

  *)
    # Do whatever you want with extra options
    echo "Unknown action '$ACTION'"
    # @todo: emit an error
    ;;
esac
