#!/bin/bash

# @todo: description

# @todo: get arguments from stdin first line

# @todo: should be obtained from script arguments
# if server-core.sh script path is not set, it is supposed that the script
# is located in the same directory as the currnt one
CURRENT_PATH="$(dirname "$0")"
# @todo: check if the file is there, emit an error otherwise
SERVER_CORE_SCRIPT_PATH="$CURRENT_PATH/server-core.sh"
# @todo: check for best way for getting stdin, maybe use 'read' in a loop
SERVER_STDIN="$(cat -)"
SERVER_CORE_STDIN=""
# positional parameters string
SERVER_CORE_PARAMETERS="$(printf "%s" "${SERVER_STDIN}" | head -1)"
SERVER_CORE_STDIN="$(printf "%s" "$SERVER_STDIN" | sed "1 d")"

if [[ $SERVER_CORE_PARAMETERS == *"--local-auth-key"* ]]; then
  exit;
fi
LOCAL_AUTH_KEY=""

auth_key_path=""
CONFIG_FILE_PATH=""
while getopts ":c:" opt; do
  case $opt in
    c)
      allow_put_archive=false
      # @todo: make sure auth_key is not empty, also auth_key_path file exists
      auth_key_path=""
      CONFIG_FILE_PATH="$OPTARG"
      source "$CONFIG_FILE_PATH"
      # @todo: $auth_key_path is supposed to be defined in the $CONFIG_FILE_PATH config file
      auth_key=$(head -n 1 "$auth_key_path")
      LOCAL_AUTH_KEY="$auth_key"
      SERVER_CORE_PARAMETERS="$SERVER_CORE_PARAMETERS --local-auth-key $LOCAL_AUTH_KEY"

      if [ "$allow_put_archive" = true ] ; then
        SERVER_CORE_PARAMETERS="$SERVER_CORE_PARAMETERS --allow-put-archive"
      fi
      ;;
  esac
done
# @todo: only echo in debug mode or to log file
#echo ""
#echo ===== SERVER WRAPPER execution =====
#echo ""

#echo SERVER_STDIN:
#echo "$SERVER_STDIN"



#echo SERVER_CORE_PARAMETERS:
#echo "$SERVER_CORE_PARAMETERS"
#echo SERVER_CORE_STDIN:
#echo "$SERVER_CORE_STDIN"

# @todo: get first string from stdin that is expected to contain script arguments,
#   pass required arguemtns to server-core.sh script, filter out unneed arguments,
#   i.e. those required only for server-wrapper.sh script


# do not use quotes for $SERVER_CORE_PARAMETERS to avoid escaping spaces
# which serve as positional parameters delimiters
# @todo: though this may break parameter values containing spaces (and maybe other chars)
#   maybe do some encoding when sending from client-wrapper.sh script
printf '%s' "$SERVER_CORE_STDIN" | /bin/bash "$SERVER_CORE_SCRIPT_PATH" $SERVER_CORE_PARAMETERS
